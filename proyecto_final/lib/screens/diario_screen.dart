import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:proyecto_final/components/MaxLinesTextInputFormatter.dart';
import 'package:proyecto_final/components/show_toast.dart';
import 'package:proyecto_final/data/diario_service.dart';


class DiarioScreen extends StatefulWidget {
  final int idPj;

  DiarioScreen(this.idPj);

  @override
  DiarioScreenState createState() => new DiarioScreenState();
}

class DiarioScreenState extends State<DiarioScreen> with TickerProviderStateMixin {

  bool saving = true;
  bool searching = false;

  TextEditingController nombreTab = TextEditingController();
  TextEditingController searchContrl = TextEditingController();
  List<String> nombresTabs = List.empty(growable: true);
  int currentTab = 0;
  int numPaginas = 0;
  int totalMatches = 0;
  int currentMatch = 0;
  bool escrito = false;
  bool btnActivado = false;
  bool hayCoincidencias = false;

  int toggle = 0;
  AnimationController _animationController;

  AnimationController _con;
  TextEditingController searchTextCntrl;

  Timer timer;

    
  final PageController pageController = PageController(initialPage: 0);

  List <TextEditingController> controllers = List.empty(growable: true);

  Map<String, dynamic> datosNotas;

  List<String> tabsConResultados = List.empty(growable: true);
  List<int> indexPagResultados = List.empty(growable: true);  

  buscar(String newQuery, tabController) {
    const duration = Duration(milliseconds: 500);
    tabsConResultados.clear();
    indexPagResultados.clear();

    if (timer != null) { 
      timer.cancel();
    }

    timer = new Timer(duration, () async{
      if (newQuery.trim() == "") {
        setState(() {
          searchTextCntrl.clear();
          totalMatches = 0;
          currentMatch = 0;
          hayCoincidencias = false;
          btnActivado = false;
        });
      }else { 
        final res = await searchWord(widget.idPj, newQuery);
        if(res["error"] != null){
          print(res["error"]);
          setState(() {
            totalMatches = 0;
            currentMatch = 0;
            hayCoincidencias = false;
            btnActivado = false;
          });
        }else {
          print(res["succesful"]);
          FocusScopeNode currentFocus = FocusScope.of(context);

          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }

          var resultados = res["succesful"];
          setState(() {
            totalMatches = resultados.length;
            currentMatch = 1;
            hayCoincidencias = true;
          });
            if(totalMatches > 1) {
              for(int i = 0; i < resultados.length; i++){
                tabsConResultados.add(resultados[i]["pestanya"]);
                indexPagResultados.add(resultados[i]["index_pag"]);
              }
              setState(() {
                btnActivado = true;
                tabController.animateTo(nombresTabs.indexWhere((nombre) => nombre == tabsConResultados[0]));
              });

              await Future.delayed(const Duration(milliseconds: 500), (){});
              
              setState(() {
                pageController.animateToPage(indexPagResultados[0], duration: const Duration(milliseconds: 400) , curve:  Curves.easeInOut);
              });
            }else {
              setState(() {
                btnActivado = false;
                tabController.animateTo(nombresTabs.indexWhere((nombre) => nombre == resultados[0]["pestanya"]));
              });
              
              await Future.delayed(const Duration(milliseconds: 500), (){});

              setState(() {
                pageController.animateToPage(resultados[0]["index_pag"], duration: const Duration(milliseconds: 400) , curve:  Curves.easeInOut);
              });
            }
         
        }
      }
    });
  }
    
  
  void cargarDatosDiario(id) async{
    final res = await getDiario(id);
      if(res["error"] != null){
        Navigator.pop(context);
      }else {
        print(Map.from(res["succesful"]));
        datosNotas = Map.from(res["succesful"]);
        for (final mapEntry in datosNotas.entries) { 
          nombresTabs.add(mapEntry.key);
          
          final value = mapEntry.value;

          value.addEntries([MapEntry("controllers" , List.empty(growable: true))]);

          for(var nota in datosNotas[mapEntry.key]["notas"]){
            TextEditingController controller = TextEditingController();
            controller.text = nota;
            datosNotas[mapEntry.key]["controllers"].add(controller);
          }
        }
        saving = false;
        setState(() {
          
        });
        
      }

      /**datosNotas.entries son todos los datos que tiene el Map. Ej: Lugares: {notas: [Hola, Adios]}), MapEntry(Personajes: {notas: [Blalala, Hehehhehehe, badadadidi]})
     * mapEntry es cada una de las iteraciones del Map. Ej Lugares: {notas: [Hola, Adios]} seria un mapEntry
     * mapEntry.value es el valor de una de las iteraciones. Ej: {notas: [Hola, Adios]}
     * mapEntry.key es la key de cada iteracion. Ej: Lugares
     */
      
    
  }

  @override
  void initState() {
    super.initState();
    cargarDatosDiario(widget.idPj);

    _animationController = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
    )..repeat();
    
    searchTextCntrl = TextEditingController();
    _con = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 375),
    );
  }

  @override
  dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Animatable<Color> background = TweenSequence<Color>([
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.green,
        end: Colors.limeAccent[400],
      ),
    ),
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.limeAccent[400],
        end: Colors.green,
      ),
    ),
  ]);

  Widget cargarPageview(data, size){
    List<Widget> listWidgets = [];

    for(int i=0; i < datosNotas[data.key]["notas"].length; i++){
        listWidgets.add(Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/paperTexture.jpg"),
              fit: BoxFit.cover
            )
          ),
          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
          child: 
            TextField(
              onChanged: (value) {
                if(value.trim() == datosNotas[data.key]["notas"][i]) {
                  escrito = false;
                }else {
                  escrito = true;
                }
              },
              controller: datosNotas[data.key]["controllers"][i],
              inputFormatters: [MaxLinesTextInputFormatter(14)],
              style: TextStyle(fontFamily: "Estonia", fontSize: 30, fontWeight: FontWeight.w600, height:1),
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(border: InputBorder.none),
            )
        ));
    }

     return Container(
        height: size.height * 0.8,
        width: size.width * 0.8,
        child: 
        PageView(
          controller: pageController,
          children: listWidgets
        )
      );
  }

  borrarPestanya(tabController){
    Size size = MediaQuery.of(context).size;
    nombreTab.text = nombresTabs[currentTab];
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: nombresTabs.length==1 ? Text("No se puede eliminar la última pestaña.") : Text('Estás a punto de eliminar la pestaña ${nombreTab.text} y todo su contenido. ¿Estás seguro de continuar?'),
          content: Container(height: size.height * 0.2),
          actions: <Widget>[
            if(nombresTabs.length != 1) ...[ 
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text('Cancelar')),
              TextButton(
                onPressed: () async{

                    final res = await deleteTab(widget.idPj, nombresTabs[currentTab]);
                    if (res["error"] != null){
                      showToast(res["error"]);
                    }else {
                      setState(() {

                        datosNotas.removeWhere((key, value) => key == nombresTabs[currentTab]);

                        nombresTabs.removeAt(tabController.index);
                        
                      });

                      if(currentTab > 0){
                        currentTab--; 
                      }
                      
                      showToast(res["succesful"]);
                      Navigator.pop(context);
                    }
                    
                },
                child: Text('Aceptar'),
              )
            ]else ...[
              TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Salir')),
            ]
          ],
        );
      }
    );
  }

  borrarContenido(nombreTab, currentPage) async{
    final res = await deleteNota(widget.idPj, nombreTab, currentPage);
    if(res["error"] != null) {
      showToast(res["error"]);
    }else {
      setState(() {
        datosNotas[nombreTab]["notas"][currentPage] = null;
        datosNotas[nombreTab]["controllers"][currentPage].text = "";
      });
    }
  }

  alertBorrarContent(nombreTab, currentPage){
    Size size = MediaQuery.of(context).size;
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Estás a punto de eliminar el contenido de esta página. \n¿Estás seguro de continuar?'),
          content: Container(height: size.height * 0.2),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('Cancelar')
            ),
            TextButton(
              onPressed: () {                  
                borrarContenido(nombreTab, currentPage);
                Navigator.pop(context);
              },
              child: Text('Aceptar'),
            )
          ],
        );
      });
  }

  @override
  Widget build(BuildContext context) {
    print(datosNotas);
    Size size = MediaQuery.of(context).size;
    
    return ModalProgressHUD(
      inAsyncCall: saving, 
      child: 
      datosNotas == null ?  Container(color: Colors.white,) :
      DefaultTabController(
        length: nombresTabs.length, 
        child: Builder(builder: (BuildContext context) {
          final TabController tabController = DefaultTabController.of(context);
          tabController.addListener(() {
            if (!tabController.indexIsChanging) {
              currentTab = tabController.index;

              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            }
          });
          return 
          Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xff6f3f1a),
            title: const Text('Diario'),
          ),
          body:
          SingleChildScrollView(child:
            Column(
              children: [
                ListView(
                  shrinkWrap: true,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          flex: 5,
                          child: 
                            Material(
                              color: Color(0xff896421),
                              child: 
                                TabBar(
                                  controller: tabController,
                                  isScrollable: true,
                                  unselectedLabelColor: Colors.white,
                                  indicator: ShapeDecoration(
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                                    color: Colors.yellow[300]
                                  ),
                                  labelColor: Colors.black,
                                  tabs: List<Widget>.generate(nombresTabs.length, (int index){
                                    return Tab(text: nombresTabs[index]);
                                  })
                                ),
                              )
                        ),
                        Expanded(
                          flex: 1,
                          child: 
                            Container(
                              color: Color(0xff92753f),
                              child:
                              PopupMenuButton(
                                color: Colors.yellow[100],
                                itemBuilder: (context) {
                                  return [
                                    PopupMenuItem(
                                      child: 
                                        Text("Añadir"),
                                        value: 1
                                    ),
                                    PopupMenuItem(
                                        child: 
                                          Text("Editar"),
                                          value: 2
                                    ),
                                    PopupMenuItem(
                                        child: 
                                          Text("Borrar"),
                                          value: 3
                                    ),
                                  ];
                                },
                                onSelected: (value) {
                                  FocusScopeNode currentFocus = FocusScope.of(context);

                                  if (!currentFocus.hasPrimaryFocus) {
                                    currentFocus.unfocus();
                                  }
                                  if (value == 1){
                                    nombreTab.text = "";
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: Text('Nombre de la pestaña'),
                                          content: Container(height: size.height * 0.2, child: Column(children: [
                                            TextField(controller: nombreTab),
                                          ],)),
                                          actions: <Widget>[
                                            TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Text('Cancelar')),
                                            TextButton(
                                              onPressed: () async{
                                                bool coincide = false;
                                                for (int i = 0; i < nombresTabs.length; i++){
                                                  if(nombresTabs[i].toLowerCase() == nombreTab.text.toLowerCase()){
                                                    coincide = true;
                                                  }
                                                }
                                                if(coincide == true){
                                                  showToast("Ya existe una pestaña con ese nombre");
                                                }else {
                                                  final res = await addPestanya(widget.idPj, nombreTab.text, "inicial");
                                                  if(res["error"] != null) {
                                                    showToast(res["error"]);
                                                  }else {
                                                    setState(() {
                                                      nombresTabs.add(nombreTab.text);
                                                      TextEditingController cntrl = TextEditingController();
                                                      datosNotas.addEntries([MapEntry(nombreTab.text , {
                                                        "notas" : [""],
                                                        "controllers" : [cntrl]
                                                      })]);
                                                      showToast(res["succesful"]);
                                                      Navigator.pop(context);
                                                    });
                                                  }
                                                }
                                                //tabController.animateTo((tabController.index + 1)); //TODO cambia a la pestaña siguiente en la que estoy. NO ES EL COMPORTAMIENTO DESEADO. Hay que hacer que vaya a la ultima pestaña añadida.
                                              },
                                              child: Text('Guardar'),
                                            )
                                          ],
                                        );
                                      }
                                    );
                                  }else if(value == 2){
                                    nombreTab.text = nombresTabs[currentTab];
                                    showDialog(
                                      context: context,
                                      builder: (context) {
                                        return AlertDialog(
                                          title: Text('Nombre de la pestaña'),
                                          content: Container(height: size.height * 0.2, child: Column(children: [
                                            TextField(controller: nombreTab),
                                          ],)),
                                          actions: <Widget>[
                                            TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);
                                                },
                                                child: Text('Cancelar')),
                                            TextButton(
                                              onPressed: () async{
                                                bool coincide = false;
                                                for (int i = 0; i < nombresTabs.length; i++){
                                                  if(nombresTabs[i].toLowerCase() == nombreTab.text.toLowerCase()){
                                                    coincide = true;
                                                  }
                                                }
                                                if(coincide == true && nombreTab.text.toLowerCase() != nombresTabs[currentTab].toLowerCase()){
                                                  showToast("Ya hay una pestaña con ese nombre");
                                                }else {
                                                  setState(() {
                                                  });
                                                  final res = await editPestanya(widget.idPj, nombresTabs[currentTab], nombreTab.text);
                                                  if(res["error"] != null){
                                                    showToast(res["error"]);
                                                  }else {
                                                    datosNotas.clear();
                                                    nombresTabs.clear();
                                                    setState(() {
                                                      cargarDatosDiario(widget.idPj); 
                                                    });
                                                    //nombresTabs[currentTab] = nombreTab.text;
                                                    showToast(res["succesful"]);
                                                    Navigator.pop(context);
                                                  }
                                                }
                                              },
                                              child: Text('Guardar'),
                                            )
                                          ],
                                        );
                                      }
                                    );
                                  }else if(value == 3){
                                    borrarPestanya(tabController);
                                  }
                                },
                              )
                            )
                        ),
                      ],
                    )
                  ],
                ),
                Row(
                  children: [
                    Container(
                      width: size.width * 0.05,
                      height: size.height * 0.7125,
                      color: Colors.brown[700],
                      ),
                    Container(
                      width: size.width * 0.95,
                      height: size.height * 0.7125,
                      child: 
                        TabBarView(
                          physics: NeverScrollableScrollPhysics(),
                          controller: tabController,
                          children: [ 
                            for (final mapEntry in datosNotas.entries)
                              cargarPageview(mapEntry, size)
                        ],)
                    )
                  ],
                ),
                Container(
                  height: size.height * 0.088,
                  color: Colors.brown,
                  child: 
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Visibility(
                          visible: !searching,
                          child: 
                            IconButton(
                              onPressed: () async{
                                nombreTab.text = nombresTabs[currentTab];
                                //final res = await addPestanya(widget.idPj, nombreTab.text, "siguiente");
                                final res = await addPagina(widget.idPj, nombreTab.text);
                                if(res["error"] != null) {
                                  showToast(res["error"]);
                                }else {
                                  setState(() {
                                    TextEditingController cntrl = TextEditingController();
                                    var listaNotas = datosNotas[nombreTab.text]["notas"].toList();
                                    var listaControllers = datosNotas[nombreTab.text]["controllers"].toList();
                                    listaNotas.add("");
                                    listaControllers.add(cntrl);
                                    datosNotas.addEntries([MapEntry(nombreTab.text , {
                                      "notas" : listaNotas,
                                      "controllers" : listaControllers
                                    })]);
                                    print(datosNotas);
                                    pageController.animateToPage(datosNotas[nombreTab.text]["notas"].length, duration: const Duration(milliseconds: 400) , curve:  Curves.easeInOut);
                                  });
                                }
                              },
                              icon: Icon(Icons.note_add_outlined),
                              iconSize: 30,
                              color: Colors.white70
                            ),
                        ),
                        AnimatedBuilder(
                        animation: _animationController,
                        builder: (context, child) {
                            return IconButton(
                              onPressed: () async {
                                if(escrito) {
                                  nombreTab.text = nombresTabs[currentTab];

                                  final res = await saveNota(widget.idPj, nombreTab.text, pageController.page.round(), datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text);

                                  if(res["error"] != null){
                                    showToast(res["error"]);
                                  }else {
                                    showToast(res["succesful"]);
                                    setState(() {
                                      escrito = false;
                                      datosNotas[nombreTab.text]["notas"][pageController.page.round()] = datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text;
                                    });
                                  }
                                }
                              },
                              icon: Icon(Icons.save),
                              iconSize: 30,
                              color:  escrito ? background
                                  .evaluate(AlwaysStoppedAnimation(_animationController.value)) : 
                                  Colors.green
                            );
                        },  
                        ),
                        Container(
                          color: const Color(0xFF0E3311).withOpacity(0.0),
                          child: Center(
                            child: Container(
                              height: 100.0,
                              width: toggle == 0 ? 50 : 250.0,
                              alignment: Alignment(-1.0, 0.0),
                              child: AnimatedContainer(
                                duration: Duration(milliseconds: 375),
                                height: 48.0,
                                width: (toggle == 0) ? 48.0 : 250.0,
                                curve: Curves.easeOut,
                                decoration: BoxDecoration(
                                  color: Colors.yellow[100],
                                  borderRadius: BorderRadius.circular(30.0),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black26,
                                      spreadRadius: -10.0,
                                      blurRadius: 10.0,
                                      offset: Offset(0.0, 10.0),
                                    ),
                                  ],
                                ),
                                child: Stack(
                                  children: [
                                    AnimatedPositioned(
                                      duration: Duration(milliseconds: 375),
                                      top: 16,
                                      left: 157,
                                      curve: Curves.easeOut,
                                      child: AnimatedOpacity(
                                        opacity: (toggle == 0) ? 0.0 : 1.0,
                                        duration: Duration(milliseconds: 200),
                                        child: AnimatedBuilder(
                                            child: Visibility(
                                              visible: hayCoincidencias, 
                                              child: Text("$currentMatch/$totalMatches", style: TextStyle(color: Colors.grey))
                                            ),
                                            builder: (context, widget) {
                                              return Transform.rotate(
                                                angle: _con.value * 2.0 * 3.1416,
                                                child: widget,
                                              );
                                            },
                                          animation: _con,
                                        ),
                                      ),
                                    ),
                                    AnimatedPositioned(
                                      duration: Duration(milliseconds: 375),
                                      right: 0.5,
                                      curve: Curves.easeOut,
                                      child: AnimatedOpacity(
                                        opacity: (toggle == 0) ? 0.0 : 1.0,
                                        duration: Duration(milliseconds: 200),
                                        child: AnimatedBuilder(
                                            child: IconButton(
                                              icon: Icon(Icons.expand_less, size: 30.0) ,
                                              onPressed: btnActivado == false ? 
                                                  null
                                                :
                                                  () async{
                                                    if(currentMatch == totalMatches) {
                                                      currentMatch = 0;
                                                    }
                                                    currentMatch++;
                                                    setState(() {
                                                      tabController.animateTo(nombresTabs.indexWhere((nombre) => nombre == tabsConResultados[currentMatch - 1]));
                                                    });

                                                    await Future.delayed(const Duration(milliseconds: 500), (){});
                                                    
                                                    setState(() {
                                                      pageController.animateToPage(indexPagResultados[currentMatch - 1], duration: const Duration(milliseconds: 400) , curve:  Curves.easeInOut);
                                                    });
                                                  },
                                            ),
                                            builder: (context, widget) {
                                              return Transform.rotate(
                                                angle: _con.value * 2.0 * 3.1416,
                                                child: widget,
                                              );
                                            },
                                          animation: _con,
                                        ),
                                      ),
                                    ),
                                    AnimatedPositioned(
                                      duration: Duration(milliseconds: 375),
                                      left: 202.5,
                                      curve: Curves.easeOut,
                                      child: AnimatedOpacity(
                                        opacity: (toggle == 0) ? 0.0 : 1.0,
                                        duration: Duration(milliseconds: 200),
                                        child: AnimatedBuilder(
                                            child: Container(height: 80, child: VerticalDivider(color: Colors.black38, thickness: 1,)),
                                            builder: (context, widget) {
                                              return Transform.rotate(
                                                angle: _con.value * 2.0 * 3.1416,
                                                child: widget,
                                              );
                                            },
                                          animation: _con,
                                        ),
                                      ),
                                    ),
                                    
                                    AnimatedPositioned(
                                      duration: Duration(milliseconds: 375),
                                      left: 170.0,
                                      curve: Curves.easeOut,
                                      child: AnimatedOpacity(
                                        opacity: (toggle == 0) ? 0.0 : 1.0,
                                        duration: Duration(milliseconds: 200),
                                        child: AnimatedBuilder(
                                            child: IconButton(
                                              icon: Icon(Icons.expand_more, size: 30.0),
                                              onPressed: btnActivado == false ? 
                                                  null
                                                :
                                                  () async{
                                                    if(currentMatch == 1) {
                                                      currentMatch = totalMatches + 1;
                                                    }
                                                    currentMatch--;
                                                    setState(() {
                                                      tabController.animateTo(nombresTabs.indexWhere((nombre) => nombre == tabsConResultados[currentMatch - 1]));
                                                    });

                                                    await Future.delayed(const Duration(milliseconds: 500), (){});
                                                    
                                                    setState(() {
                                                      pageController.animateToPage(indexPagResultados[currentMatch - 1], duration: const Duration(milliseconds: 400) , curve:  Curves.easeInOut);
                                                    });
                                                  },
                                            ),
                                            builder: (context, widget) {
                                              return Transform.rotate(
                                                angle: _con.value * 2.0 * 3.1416,
                                                child: widget,
                                              );
                                            },
                                          animation: _con,
                                        ),
                                      ),
                                    ),
                                    AnimatedPositioned(
                                      duration: Duration(milliseconds: 375),
                                      left: (toggle == 0) ? 20.0 : 40.0,
                                      curve: Curves.easeOut,
                                      top: 11.0,
                                      child: AnimatedOpacity(
                                        opacity: (toggle == 0) ? 0.0 : 1.0,
                                        duration: Duration(milliseconds: 200),
                                        child: Container(
                                          height: 23.0,
                                          width: 130.0,
                                          child: TextField(
                                            onChanged: (value) {
                                                buscar(value, tabController);
                                            },
                                            inputFormatters: [
                                              LengthLimitingTextInputFormatter(11),
                                            ],
                                            controller: searchTextCntrl,
                                            cursorRadius: Radius.circular(10.0),
                                            cursorWidth: 2.0,
                                            cursorColor: Colors.black,
                                            decoration: InputDecoration(
                                              floatingLabelBehavior: FloatingLabelBehavior.never,
                                              labelText: 'Buscar...',
                                              labelStyle: TextStyle(
                                                color: Color(0xff5B5B5B),
                                                fontSize: 17.0,
                                                fontWeight: FontWeight.w500,
                                              ),
                                              alignLabelWithHint: true,
                                              border: OutlineInputBorder(
                                                borderRadius: BorderRadius.circular(20.0),
                                                borderSide: BorderSide.none,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Material(
                                      color: Colors.yellow[100],
                                      borderRadius: BorderRadius.circular(30.0),
                                      child: IconButton(
                                        splashRadius: 19.0,
                                        icon: Icon(Icons.search),
                                        iconSize:  18.0,
                                        onPressed: () {
                                          setState(
                                            () {
                                              if (toggle == 0) {
                                                searching = true;
                                                toggle = 1;
                                                _con.forward();
                                              } else {
                                                searching = false;
                                                toggle = 0;
                                                searchTextCntrl.clear();
                                                _con.reverse();
                                              }
                                            },
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                       
                        IconButton(
                          onPressed: () async{
                            nombreTab.text = nombresTabs[currentTab];
                        
                            /**
                             * Si en la pestaña actual solo hay una página, si no hay texto y yo no he escrito nada, borrará la pestaña. Si he escrito algo, borrará lo que he escrito. 
                             * 
                             * Si en la página hay texto y escribo algo, solo se borrará lo que he escrito y el texto volverá a ser el que había antes. Si escribo algo y el texto es igual al que había antes, se me avisará de que voy a borrar el contenido de la página.
                             * 
                             * Si hay más de una página y el contenido de una de ellas está vacío y no he escrito nada, se borrará la página, si he escrito algo, se borrará el texto.
                             * 
                             * Si ya había texto y escribo algo distinto, al borrar, el contenido de la página volverá a ser el texto que había antes. Si no escribo o escribo lo mismo que había antes, se borrar el contenido de la página.
                             * 
                             */
                            if(datosNotas[nombreTab.text]["notas"].length == 1) {
                              if(datosNotas[nombreTab.text]["notas"][0] == null || datosNotas[nombreTab.text]["notas"][0] == ""){
                                if(datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text == "" || datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text == null){
                                  borrarPestanya(tabController);
                                }else {
                                  datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text = "";
                                  escrito = false;
                                }
                              }else {
                                if(datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text != datosNotas[nombreTab.text]["notas"][pageController.page.round()]){

                                    datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text = datosNotas[nombreTab.text]["notas"][pageController.page.round()];
                                    escrito = false;

                                }else {
                                  alertBorrarContent(nombreTab.text, pageController.page.round());
                                }
                              }
                            }else {
                              if(datosNotas[nombreTab.text]["notas"][pageController.page.round()] == null || datosNotas[nombreTab.text]["notas"][pageController.page.round()] == ""){
                                if(datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text == "" || datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text == null){
                                  final res = await deletePagina(widget.idPj, nombreTab.text, pageController.page.round());
                                  if(res["error"] != null){
                                    showToast(res["error"]);
                                  }else {
                                    setState(() {
                                      var listaNotas = datosNotas[nombreTab.text]["notas"].toList();
                                      var listaControllers = datosNotas[nombreTab.text]["controllers"].toList();
                                      listaNotas.removeAt(pageController.page.round());
                                      listaControllers.removeAt(pageController.page.round());
                                      datosNotas.addEntries([MapEntry(nombreTab.text , {
                                        "notas" : listaNotas,
                                        "controllers" : listaControllers
                                      })]);
                                      print(datosNotas);
                                    });
                                  }
                                }else {
                                  datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text = "";
                                  escrito = false;
                                }
                              }else {
                                if(datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text != datosNotas[nombreTab.text]["notas"][pageController.page.round()]){

                                    datosNotas[nombreTab.text]["controllers"][pageController.page.round()].text = datosNotas[nombreTab.text]["notas"][pageController.page.round()];
                                    escrito = false;
                                }else {
                                  alertBorrarContent(nombreTab.text, pageController.page.round());
                                }
                              }
                            }

                          }, 
                          icon: Icon(Icons.delete_forever),
                          iconSize: 30,
                          color: Colors.red[300]
                        ),
                        
                      ],
                    )
                  /*:
                    Row(
                      children: [
                        AnimSearchBar(
                          width: 400,
                          textController: searchContrl,
                          onSuffixTap: () {
                            setState(() {
                              searchContrl.clear();
                            });
                          },
                        )
                      ],
                    )*/
                )
              ],
            ),
          ));
        })
      )
    );
  }
}