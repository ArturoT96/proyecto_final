import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:proyecto_final/components/show_toast.dart';
import 'package:proyecto_final/data/inventario_service.dart';

class ObjetosScreen extends StatefulWidget {

  final int idPj;
  final int idPag;

  ObjetosScreen(this.idPj, this.idPag);
  
  @override
  ObjetosScreenState createState() => new ObjetosScreenState();
}

class ObjetosScreenState extends State<ObjetosScreen> {
  
  String title;

  int count = 0;

  List<Item> items = List.empty(growable: true);



  initState(){
    super.initState();

    switch (widget.idPag) {
      case 0:
          title = "General";
          break;
      case 1:
          title = "Pociones";
          break;
      case 2:
          title = "Objetos Mágicos";
          break;
      case 3:
          title = "Armamentos";
          break;
    }

    traerDatosObj();

  }

  traerDatosObj() async{
    
    dynamic res = await getDatosObj(widget.idPj, widget.idPag);

    if(res != null || res != ""){
      var id;
      var nombre;
      var descripcion;
      var peso;
      var valor;
      var tipoMoneda;

      for(int i = 0; i < res["succesful"]["nombres"].length; i++){
        Map<String, dynamic> pjStats;

        id = res["succesful"]["ides"][i];
        nombre = res["succesful"]["nombres"][i];

        if(res["succesful"]["descripciones"][i] == null){
          descripcion = " ";
        }else {
          descripcion = res["succesful"]["descripciones"][i];
        }

        if(res["succesful"]["pesos"][i] == null){
          peso = 0;
        }else {
          peso = res["succesful"]["pesos"][i];
        }

        if(res["succesful"]["valores"][i] == null){
          valor = 0;
        }else {
          valor = res["succesful"]["valores"][i];
        }

        if(res["succesful"]["tiposMonedas"][i] == null){
          tipoMoneda = "po";
        }else {
          tipoMoneda = res["succesful"]["tiposMonedas"][i];
        }

       
        pjStats = {
          "id" : id,
          "nombre" : nombre,
          "descripcion" : descripcion,
          "valor" : valor,
          "peso" : peso,
          "tipoMoneda" : tipoMoneda,
        };

        setState(() {
          items.add(Item(nombre, descripcion, valor.toString(), peso.toString(), i, items, removeItem, refresh, tipoMoneda, widget.idPj, pjStats, index: id));
        });
      }
    }
  }

  void removeItem(index, lista) {
    setState(() {
      lista.removeWhere((item) => item.index == index);
    });
  }

  FutureOr refresh(dynamic value){
    items.clear();
    traerDatosObj();
  }


  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[400],
        title: Text(title),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        child: 
          Column(
            children: [
              Row(
                children: [
                  Text("Lista de Objetos"),
                  IconButton(
                    icon: Icon(
                      Icons.add_circle_outline, 
                      color: Colors.green,), 
                    onPressed: (){
                      setState(() {
                        count = items.length;
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return MyDialogContent(items, setState, removeItem, "anyadir", widget.idPj, widget.idPag, null);
                          }
                        ).then(refresh);
                      });
                    }
                  )
              ],),
              items.isNotEmpty ? 
                ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true, 
                    children: <Widget>[...items],
                )
                : Text("No hay personajes creados",
                    style: 
                      TextStyle(
                        fontStyle: FontStyle.italic,
                        color: Colors.blueGrey[300]
                      )     
                  )
          ],)
      ),
    );
  }
}

class Item extends StatefulWidget {
  final String nombre;
  final String descripcion;
  final String valor;
  final String peso;
  final String moneda;
  final int idPj;
  final int index;
  final int count;
  final pjStats;
  final List<Item> lista;
  final Function(dynamic, dynamic) removeItem;
  final Function refresh;

  Item(this.nombre, this.descripcion, this.valor, this.peso, this.count, this.lista, this.removeItem, this.refresh, this.moneda, this.idPj, this.pjStats, {Key key, @required this.index})
      : super(key: key);

  @override
  ItemState createState() => new ItemState();
}

class ItemState extends State<Item>{

  bool isSubtitle = false;
  bool isDescripcion = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.descripcion == "" || widget.descripcion == " "){
      isDescripcion = false;
    }else {
      isDescripcion = true;
    }
  }
  @override
  Widget build(BuildContext context){
    Size size = MediaQuery.of(context).size;
    return 
    Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child:
        Padding(
          padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          child:
          IntrinsicHeight (child: 
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start, //change here don't //worked
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(widget.valor.toString() + " " + widget.moneda),
                    SizedBox(height: size.height * 0.02,),
                    Text(widget.peso.toString() + " lb")
                  ],
                ),
                SizedBox(width: size.width * 0.05,),
                InkWell(
                  child:
                    Container(
                      width: size.width * 0.30,
                      child: 
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(widget.nombre, style: TextStyle(fontWeight: FontWeight.bold)),
                          SizedBox(height: size.height * 0.01,),
                              isSubtitle? 
                          Text(widget.descripcion)
                          :
                            isDescripcion?
                            Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),)
                            :
                            Text(""),
                        ],
                      ),
                    ),
                    onTap: (){
                      setState(() {
                        isSubtitle = !isSubtitle;
                      });
                    },
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: 
                    IconButton(
                      icon: Icon(Icons.edit, color: Colors.grey), 
                      onPressed: () async{
                        showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return MyDialogContent(widget.lista, setState, widget.removeItem, "editar", widget.index, null, widget.pjStats);
                          }
                        ).then(widget.refresh);
                      },
                    ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: 
                    VerticalDivider(color: Colors.grey[400],)
                ,),
                Align(
                  alignment: Alignment.centerRight,
                  child: 
                  IconButton(
                      icon: Icon(Icons.delete, color: Colors.red), 
                      onPressed: () async{
                        setState(() {
                          showDialog(context: context, builder: (BuildContext builder){
                            return 
                              AlertDialog(
                                contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                                insetPadding: EdgeInsets.all(10),
                                title: Text("Estás a punto de borrar ${widget.nombre} y todo su contenido. ¿Estás seguro que quieres continuar?"), 
                                content: Container(height: size.height * 0.02),
                                actionsAlignment: MainAxisAlignment.center,
                                actions: 
                                  <Widget>[
                                    ElevatedButton(
                                        onPressed: () async{
                                          final dynamic res = await eliminarObjeto(widget.index);
                                          if(res["succesful"] != null){
                                            widget.removeItem(widget.index, widget.lista);
                                            Navigator.pop(context);
                                            showToast(res['succesful']);
                                          }else{
                                            showToast(res['error']);
                                          }
                                        }, 
                                        child: Text("Eliminar")
                                      ),
                                      ElevatedButton(
                                        onPressed: () async{
                                          Navigator.pop(context);
                                        }, 
                                        child: Text("Cancelar")
                                      )
                                  ],     
                              );
                          });
                        });
                      },
                    )
                ,)
            ],)
          ,)
        ,)
    );
  }
}

class MyDialogContent extends StatefulWidget {
  final items;
  final tipo;
  final idPJ;
  final idPag;
  final Map<String, dynamic> pjStats;
  final Function state;
  final Function removeItem;
  MyDialogContent(this.items, this.state, this.removeItem, this.tipo, this.idPJ, this.idPag, this.pjStats, {
    Key key,
  }): super(key: key);

  @override
  _MyDialogContentState createState() => new _MyDialogContentState();
}

class _MyDialogContentState extends State<MyDialogContent> {

  final _formKey = GlobalKey<FormState>();

  String tipoMoneda;


  TextEditingController nombre = TextEditingController();
  TextEditingController descripcion = TextEditingController();
  TextEditingController valor = TextEditingController();
  TextEditingController peso = TextEditingController();

  List<DropdownMenuItem<dynamic>> monedas=[
    DropdownMenuItem(value: "ppt",child: Text("Platino")),
    DropdownMenuItem(value: "po",child: Text("Oro")), 
    DropdownMenuItem(value: "pe",child: Text("Electrum")),
    DropdownMenuItem(value: "pp",child: Text("Plata")), 
    DropdownMenuItem(value: "pc",child: Text("Cobre")),
  ];

  initState(){
    initControllers();

  }

  initControllers(){
    if(widget.tipo == "editar"){
      nombre.text = widget.pjStats["nombre"];
      descripcion.text = widget.pjStats["descripcion"];
      peso.text = widget.pjStats["peso"].toString();
      valor.text = widget.pjStats["valor"].toString();
      tipoMoneda = widget.pjStats["tipoMoneda"];
    
      nombre.selection = TextSelection.fromPosition(TextPosition(offset: nombre.text.length));
      descripcion.selection = TextSelection.fromPosition(TextPosition(offset: descripcion.text.length));
      peso.selection = TextSelection.fromPosition(TextPosition(offset: peso.text.length));
      valor.selection = TextSelection.fromPosition(TextPosition(offset: valor.text.length));
      
    }
  }

  void addItem(nombre, descripcion, valor, peso, id, lista, count, moneda, pjStats, refresh){
    lista.add(Item(nombre, descripcion, valor, peso, count, lista, widget.removeItem, refresh, moneda, widget.idPJ, pjStats, index:id,));  
  }

  Map<String, dynamic> pjStats;
    
    
  Widget build(BuildContext context){
    Size size = MediaQuery.of(context).size;
    return StatefulBuilder(
      builder: (context, setState) {
        return AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(40, 40, 40, 20),
          content: SingleChildScrollView(
            child: Column(
              children: [
                Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text("Nombre"),
                      TextFormField(
                        textAlign: TextAlign.center,
                        controller: nombre,
                        validator: (value) {
                          if (value == null || value == "") {
                            return "Campo obligatorio";
                          }
                          return null;
                        }
                      ),
                      SizedBox(height: size.height * 0.03,),
                      Text("Descripción"),
                      TextFormField(
                          textAlign: TextAlign.center,
                          controller: descripcion
                      ),
                      SizedBox(height: size.height * 0.03,),
                      DropdownButtonFormField(
                        decoration: InputDecoration(labelText: "Tipo de moneda:\n\n", labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                        value: tipoMoneda,
                        items: monedas, 
                        hint: Text("Selecciona"), 
                        onChanged: (value){
                          setState(() {
                            tipoMoneda = value;
                          });
                      }),
                      SizedBox(height: size.height * 0.03,),
                      Text("Valor"),
                      TextFormField(
                        textAlign: TextAlign.center,
                        keyboardType: TextInputType.number,
                        controller: valor,
                      ),
                      SizedBox(height: size.height * 0.03,),
                      Text("Peso"),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        textAlign: TextAlign.center,
                        controller: peso,
                      ),
                      Divider(color: Colors.grey[200]),
                    ]
                  )
                ),
              ],
            ),
          ),
          actions: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                  child: widget.tipo == "anyadir" ? Text("Crear Objeto") : Text("Editar Objeto"),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {

                      if(widget.tipo == "anyadir"){
                        print(widget.idPJ);
                        final res = await anyadirObjeto(widget.idPJ, widget.idPag, nombre.text, descripcion.text, valor.text, tipoMoneda, peso.text);

                        if(res["error"] != null){
                          showToast(res["error"]);
                        }else {
                          Navigator.pop(context);
                          showToast("Objeto añadido correctamente");
                        }
                        
                      }else if(widget.tipo == "editar"){
                        final dynamic res = await editarObjeto(widget.pjStats["id"], nombre.text, descripcion.text, valor.text, tipoMoneda, peso.text);
                        print(res);
                          if(res["error"] != null){
                            showToast(res["error"]);
                          }else {
                            if (res['succesful'] != null) {
                              Navigator.pop(context);
                              showToast("Objeto editado correctamente");
                            }
                        }
                      }
                    }
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red,
                    onPrimary: Colors.white, // foreground
                  )
                ),
                SizedBox(width: size.width * 0.05,),
                ElevatedButton(
                  onPressed: (){
                    Navigator.pop(context);
                  }, 
                  child: Text("Cancelar"),
                  style: ElevatedButton.styleFrom(
                    primary: Colors.red,
                    onPrimary: Colors.white, // foreground
                  )
                )
              ],
            )
          ],
        );
      });
  }
}