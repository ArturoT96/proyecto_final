import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:bordered_text/bordered_text.dart';
import 'package:proyecto_final/components/show_toast.dart';
import 'package:proyecto_final/data/pj_service.dart';
import 'package:proyecto_final/routes_api_http/webclient.dart';
import 'package:proyecto_final/screens/diario_screen.dart';
import 'package:proyecto_final/screens/inventario_screen.dart';

class PJScreen extends StatefulWidget {
  final Image imagen;

  final Map<String, dynamic> pjStats;

  PJScreen(this.imagen, this.pjStats);

  @override
  PJScreenState createState() => new PJScreenState();
}

class PJScreenState extends State<PJScreen> with WidgetsBindingObserver {

  TextEditingController numDadosCtrl = TextEditingController();
  TextEditingController bonusDadosCtrl = TextEditingController();

  int numEstados = 0;

  Map<String, bool> estados;

  int id;
  String nombre;
  String clase;
  String nivel;
  int maxHP = 0;
  int currentHP = 0;
  int temporalHP = 0;
  int clase_armadura;

  double percent = 0;

  bool envenenado = false;
  bool cegado = false;
  bool paralizado = false;
  bool petrificado = false;
  bool tumbado = false;
  bool agarrado = false;
  bool encantado = false;
  bool ensordecido = false;
  bool asustado = false;
  bool incapacitado = false;
  bool invisible = false;
  bool restringido = false;
  bool aturdido = false;
  bool inconsciente = false;
  bool exhausto = false;

  bool subtitleEnvenenado = false;
  bool subtitleCegado = false;
  bool subtitleParalizado = false;
  bool subtitlePetrificado = false;
  bool subtitleTumbado = false;
  bool subtitleAgarrado = false;
  bool subtitleEncantado = false;
  bool subtitleEnsordecido = false;
  bool subtitleAsustado = false;
  bool subtitleIncapacitado = false;
  bool subtitleInvisible = false;
  bool subtitleRestringido = false;
  bool subtitleAturdido = false;
  bool subtitleInconsciente = false;
  bool subtitleExhausto = false;

  bool dadosVisible = false;
  bool dadosOpen = false;

  int peso_equipo;
  int p_oro;
  int p_plata;
  int p_cobre;
  int p_platino;
  int p_electrum;

  String userName;

  Map<String,int> dataSalud;


  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    numEstados = 0;

    numDadosCtrl.text = "1d";
    bonusDadosCtrl.text = "+0";

    id = widget.pjStats["id"];
    nombre = widget.pjStats["nombre"];
    clase = widget.pjStats["clase"];
    nivel = widget.pjStats["nivel"];

    widget.pjStats["clase_armadura"] is String ? 
      clase_armadura = int.parse(widget.pjStats["clase_armadura"]) 
      : 
      clase_armadura = widget.pjStats["clase_armadura"];

    getEstados();
    

    estados = {"envenenado" : envenenado,
      "cegado" : cegado,
      "paralizado" : paralizado,
      "petrificado" : petrificado,
      "tumbado" : tumbado,
      "agarrado" : agarrado,
      "encantado" : encantado,
      "ensordecido" : ensordecido,
      "asustado" : asustado,
      "incapacitado" : incapacitado,
      "invisible" : invisible,
      "restringido" : restringido,
      "aturdido" : aturdido,
      "inconsciente" : inconsciente,
      "exhausto" : exhausto,
    };

   /* if(envenenado){
      numEstados++;
    }
      
    if(cegado){
      numEstados++;
    }
    if(paralizado){
      numEstados++;
    }
    if(petrificado){
      numEstados++;
    }
    if(tumbado){
      numEstados++;
    }
    if(encantado){
      numEstados++;
    }
    if(ensordecido){
      numEstados++;
    }
    if(asustado){
      numEstados++;
    }
    if(incapacitado){
      numEstados++;
    }
    if(invisible){
      numEstados++;
    }
    if(restringido){
      numEstados++;
    }
    if(aturdido){
      numEstados++;
    }
    if(inconsciente){
      numEstados++;
    }
    if(exhausto){
      numEstados++;
    }*/

    if(currentHP > maxHP){
      currentHP = maxHP;
    }

  }


  getEstados() async{
    final res = await traerEstados(id);
    if(res["error"] != null){
      showToast(res["error"]);
    }else {
      var respuesta = res["succesful"];

      setState(() {
        
        maxHP = respuesta["salud_max"];
        currentHP = respuesta["salud_actual"];
        temporalHP = respuesta["salud_temp"];

        print(maxHP);
        
        envenenado = respuesta["envenenado"];
        cegado = respuesta["cegado"];
        paralizado = respuesta["paralizado"];
        petrificado = respuesta["petrificado"];
        tumbado = respuesta["tumbado"];
        agarrado = respuesta["agarrado"];
        encantado = respuesta["encantado"];
        ensordecido = respuesta["ensordecido"];
        asustado = respuesta["asustado"];
        incapacitado = respuesta["incapacitado"];
        invisible = respuesta["invisible"];
        restringido = respuesta["restringido"];
        aturdido = respuesta["aturdido"];
        inconsciente = respuesta["inconsciente"];
        exhausto = respuesta["exhausto"];
      });

    }
  }
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  
  void didChangeAppLifecycleState(AppLifecycleState state) async{
    switch (state) {
      case AppLifecycleState.paused:
        print('paused');
        estados = {"envenenado" : envenenado,
          "cegado" : cegado,
          "paralizado" : paralizado,
          "petrificado" : petrificado,
          "tumbado" : tumbado,
          "agarrado" : agarrado,
          "encantado" : encantado,
          "ensordecido" : ensordecido,
          "asustado" : asustado,
          "incapacitado" : incapacitado,
          "invisible" : invisible,
          "restringido" : restringido,
          "aturdido" : aturdido,
          "inconsciente" : inconsciente,
          "exhausto" : exhausto,
        };
        final res = await editarEstados(id, estados);
        if(res['error'] != null){
          showToast(res["error"]);
        }
        break;
      case AppLifecycleState.detached:
        print('detached');
        break;
      default:
    }
  }

  Future<void> _dialogCall(BuildContext context) {
    return  showDialog(
        context: context,
        builder: (BuildContext context) {
          return MyDialogContent(currentHP, temporalHP, maxHP, id);
        }).then(refresh);
  }

  FutureOr refresh(dynamic value){
    getEstados();
  }

  tirarDados(max){
    int numDados = int.parse(numDadosCtrl.text.replaceAll("d", ""));
    int bonus = int.parse(bonusDadosCtrl.text.replaceAll("+", ""));
    String bonusTxt = "";
    int tirada = 0;
    int total = 0;
    List<int> tiradas = List.empty(growable: true);;

    for(int i = 0; i < numDados; i++ ){
      tirada = (Random().nextInt(max) + 1).round();
      total = total + tirada;
      tiradas.add(tirada);
    }

    if(bonus != 0){
      bonusTxt = "+"+bonus.toString();
    }

    total = total + bonus;


    showDialog(context: context, builder: (BuildContext context){
      Size size = MediaQuery.of(context).size;
      return Container(
        height: size.height * 0.5,
        child:
          AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
            insetPadding: EdgeInsets.fromLTRB(60, 0, 60,0),
            backgroundColor: Colors.grey[900],
            content: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(numDadosCtrl.text+"$max"+bonusTxt, style: TextStyle(color: Colors.white),),
                SizedBox(height: size.height * 0.02),
                Text("$total", style: TextStyle(color: Colors.white, fontSize: 80)),
                SizedBox(height: size.height * 0.02),
                Text(tiradas.join(", "), style: TextStyle(color: Colors.white))
              ]
            ),
          )
        );
    });
  }


  Widget build(BuildContext context){
    print(numEstados);
    print(envenenado);

    Size size = MediaQuery.of(context).size;
    if(currentHP >= 0){
      percent = currentHP/maxHP;
    }else {
      percent = 0;
    }

    return WillPopScope(
        onWillPop: () async {
            if(_scaffoldKey.currentState.isDrawerOpen) {
              Navigator.pop(context); // closes the drawer if opened
              return Future.value(false);
            }else {
              estados = {"envenenado" : envenenado,
                "cegado" : cegado,
                "paralizado" : paralizado,
                "petrificado" : petrificado,
                "tumbado" : tumbado,
                "agarrado" : agarrado,
                "encantado" : encantado,
                "ensordecido" : ensordecido,
                "asustado" : asustado,
                "incapacitado" : incapacitado,
                "invisible" : invisible,
                "restringido" : restringido,
                "aturdido" : aturdido,
                "inconsciente" : inconsciente,
                "exhausto" : exhausto,
              };
              final res = await editEstados(id, estados);
              if(res["error"] != null){
                showToast(res["error"]);
                return false;
              }else {
                showToast(res["succesful"]);
                return true;
              } // Action to perform on back pressed 
            }
        }, 
        child: Scaffold(
        resizeToAvoidBottomInset: false,
        drawerEnableOpenDragGesture: false,
        key: _scaffoldKey,
        drawer: 
          Container(
            width: size.width * 0.9,
            child: 
              Drawer(
                child:
                  ListView(
                    children: [
                      Container(
                        height: 60,
                        child: 
                        DrawerHeader(
                          decoration: 
                            BoxDecoration(
                              color: 
                                Colors.amber[700]
                            ),
                          child: 
                            Text("CONDICIONES")
                        ),
                      ),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/poison.png")),
                        title: 
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5), child: Text("Envenenado")
                          ),
                        subtitle: subtitleEnvenenado? 
                          Text("\u2022 Una criatura envenenada tiene desventaja en las tiradas de ataque y en las pruebas de característica.", )
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: envenenado, onChanged: (value) async{
                          /*final res = await editarEstado(id, "envenenado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              envenenado = value;
                              if(envenenado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleEnvenenado = !subtitleEnvenenado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/blind.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Cegado"),
                        ),
                        subtitle: subtitleCegado?
                          Text("\u2022 Una criatura cegada no puede ver y automáticamente fallara cualquier prueba de habilidad que requiera de la visión.\n\n\u2022 Las tiradas de ataque de la criatura sufren desventaja y los ataques que se realicen contra la criatura gozaran de ventaja.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: cegado, onChanged: (value) async{
                          /*final res = await editarEstado(id, "cegado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              cegado = value;
                              if(cegado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                          /*setState(() {
                            cegado = value;
                          });*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleCegado = !subtitleCegado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/enchanted-mirror.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Encantado"),
                        ),
                        subtitle: subtitleEncantado?
                          Text("\u2022 Una criatura encantada no puede atacar a su encantador ni afectarlo con efectos mágicos.\n\n\u2022 El encantador tiene ventaja en cualquier prueba de habilidad para interactuar socialmente con la criatura.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: encantado, onChanged: (value) async{
                          /*final res = await editarEstado(id, "encantado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              encantado = value;
                              if(encantado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleEncantado = !subtitleEncantado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/deafness.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Ensordecido"),
                        ),
                        subtitle: subtitleEnsordecido?
                          Text("\u2022 Una criatura ensordecida no puede oír y fallara automáticamente cualquier prueba de habilidad que requiere del oído.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: ensordecido, onChanged: (value) async{
                          /*final res = await editarEstado(id, "ensordecido", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              ensordecido = value;
                              if(ensordecido){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleEnsordecido = !subtitleEnsordecido;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/fear.png")),
                        title:  Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Asustado"),
                        ),
                        subtitle: subtitleAsustado?
                        Text("\u2022 Una criatura asustada tiene desventaja en las pruebas de habilidad y tiradas de ataque siempre y cuando la fuente de su temor este en su línea de visión.\n\n\u2022 Una criatura asustada no puede acercarse de forma voluntaria a la fuente de su temor.")
                        :
                        Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: asustado, onChanged: (value) async{
                          /*final res = await editarEstado(id, "asustado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              asustado = value;
                              if(asustado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleAsustado = !subtitleAsustado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/self-restraint.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Agarrado"),
                        ),
                        subtitle: subtitleAgarrado?
                          Text("\u2022 Una criatura agarrada no puede moverse y tampoco podrá beneficiarse de ningún bono que favorezca a su movimiento.\n\n\u2022 La condición termina cuando la criatura que la provoca resulta incapacitada o simplemente decide terminar con el agarre.\n\n\u2022 La condición también termina cuando un efecto aleja del alcance de su victima a la criatura que ejerce el agarre, como sucedería si la criatura es empujada por un hechizo de “onda de trueno”.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: agarrado, onChanged: (value) async{
                          /*final res = await editarEstado(id, "agarrado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              agarrado = value;
                              if(agarrado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleAgarrado = !subtitleAgarrado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/tied.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Incapacitado"),
                        ),
                        subtitle: subtitleIncapacitado?
                          Text("\u2022 Una criatura incapacitada no puede realizar acciones ni reacciones.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: incapacitado, onChanged: (value) async{
                          /*final res = await editarEstado(id, "incapacitado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              incapacitado = value;
                              if(incapacitado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleIncapacitado = !subtitleIncapacitado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/invisible.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Invisible"),
                        ),
                        subtitle: subtitleInvisible?
                          Text("\u2022 Una criatura invisible es imposible de ver sin la ayuda de la magia o de un sentido especial.\n\n\u2022 La localización de la criatura puede ser detectada por el ruido que emita o por el rastro que deje.\n\n\u2022 Las tiradas de ataque de la criatura tienen ventaja y las tiradas de ataque en contra de la criatura tienen desventaja")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: invisible, onChanged: (value) async{
                          /*final res = await editarEstado(id, "invisible", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              invisible = value;
                              if(invisible){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                        /* }else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleInvisible = !subtitleInvisible;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/lightning.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Paralizado"),
                        ),
                        subtitle: subtitleParalizado?
                          Text("\u2022 Una criatura paralizada se considera incapacitada y no puede moverse ni hablar.\n\n\u2022 La criatura automáticamente fallara cualquier tirada de salvación de fuerza o destreza.\n\n\u2022 Las tiradas de ataque contra la criatura gozan de ventaja.\n\n\u2022 Cualquier ataque exitoso contra la criatura será considerado un golpe critico siempre y cuando el atacante este a 5 o menos pies de la criatura paralizada.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: paralizado, onChanged: (value) async{
                          /*final res = await editarEstado(id, "paralizado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              paralizado = value;
                              if(paralizado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleParalizado = !subtitleParalizado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/sculpture(1).png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Petrificado"),
                        ),
                        subtitle: subtitlePetrificado?
                          Text("\u2022 Una criatura petrificada es transformada, junto a todo lo que porte (excepto objetos mágicos), en una sustancia sólida e inanimada (comúnmente roca). Su peso aumenta 10 veces y su envejecimiento se detiene.\n\n\u2022 La criatura se considera incapacitada, por ende no podrá hablar ni moverse, y tampoco será consciente de lo que ocurra a su alrededor.\n\n\u2022 Las tiradas de ataque contra la criatura gozaran de ventaja.\n\n\u2022 La criatura tiene resistencia a todo el daño.\n\n\u2022 La criatura es inmune al veneno y a la enfermedad. En caso de estar envenenado o enfermo antes de ser petrificado, el veneno u enfermedad se suspende, pero no es neutralizado/a.\n\n\u2022 La criatura automáticamente fallara cualquier tirada de salvación de fuerza o destreza.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: petrificado, onChanged: (value) async{
                        /* final res = await editarEstado(id, "petrificado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              petrificado = value;
                              if(petrificado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                        /* }else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitlePetrificado = !subtitlePetrificado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/yoga.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Tumbado")
                        ),
                        subtitle: subtitleTumbado?
                          Text("\u2022 En caso de estar consciente, la única opción de movimiento de una criatura tumbada es arrastrarse, a menos que esta se pare y termine la condición.\n\n\u2022 La criatura tiene desventaja en sus tiradas de ataque.\n\n\u2022 Una tirada de ataque contra la criatura gozara de ventaja siempre y cuando el atacante este a 5 o menos pies de la criatura tumbada, en caso de estar a más de 5 pies de distancia, el ataque tendrá desventaja.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: tumbado, onChanged: (value) async{
                        /* final res = await editarEstado(id, "tumbado", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              tumbado = value;
                              if(tumbado){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                        /* }else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleTumbado = !subtitleTumbado;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/rope.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Restringido"),
                        ),
                        subtitle: subtitleRestringido?
                          Text("\u2022 Una criatura restringida no puede moverse y tampoco podrá beneficiarse de ningún bono que favorezca a su movimiento.\n\n\u2022 Las tiradas de ataque de la criatura sufren desventaja y los ataques que se realicen contra la criatura gozaran de ventaja.\n\n\u2022 La criatura tendrá desventaja en las tiradas de salvación de destreza.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: restringido, onChanged: (value) async{
                        /* final res = await editarEstado(id, "restringido", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              restringido = value;
                              if(restringido){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleRestringido = !subtitleRestringido;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/dizziness.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Aturdido"),
                        ),
                        subtitle: subtitleAturdido?
                          Text("\u2022 Una criatura aturdida se considera incapacitad, no podrá moverse y hablara con dificultad.\n\n\u2022 La criatura automáticamente fallara las tiradas de salvación de destreza y fuerza.\n\n\u2022 Las tiradas de ataque contra la criatura gozaran de ventaja.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: aturdido, onChanged: (value) async{
                          /*final res = await editarEstado(id, "aturdido", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              aturdido = value;
                              if(aturdido){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleAturdido = !subtitleAturdido;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/dead.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Inconsciente"),
                        ),
                        subtitle: subtitleInconsciente?
                          Text("\u2022 La criatura se considera incapacitada, por ende no podrá hablar ni moverse, y tampoco será conciente de lo que ocurra a su alrededor.\n\n\u2022 La criatura soltara cualquier cosa que este sosteniendo y caerá tumbada.\n\n\u2022 La criatura automáticamente fallara cualquier tirada de salvación de fuerza o destreza.\n\n\u2022 Las tiradas de ataque contra la criatura gozaran de ventaja.\n\n\u2022 Todo ataque que impacte a la criatura se considerara un golpe critico siempre y cuando la criatura este a 5 o menos pies de distancia de la criatura.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: inconsciente, onChanged: (value) async{
                          /*final res = await editarEstado(id, "inconsciente", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              inconsciente = value;
                              if(inconsciente){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleInconsciente = !subtitleInconsciente;
                          });
                        },
                      ),
                      Divider(),
                      ListTile(
                        minVerticalPadding: 10,
                        leading: Tab(icon: Image.asset("assets/icons/tiredness.png")),
                        title: Padding(
                          padding: const EdgeInsets.only(bottom: 5), child: Text("Exhausto"),
                        ),
                        subtitle: subtitleExhausto?
                          Text("Si una criatura que ya sufre la condición de exhausto, es afectada por otro efecto que cause la misma condición, su nivel actual de cansancio se incrementa la cantidad indicada en la descripción del efecto. Una criatura exhausta sufre la penalización de su respectivo nivel de cansancio y también la de los anteriores, por ejemplo, una criatura que sufre de la condición de exhausto en el grado 2, reducirá su movimiento a la mitad y también tendrá desventaja en las pruebas de habilidad. Un efecto que remueva el cansancio, reducirá la cantidad especificada por el efecto de grados de cansancio en la criatura. Para que la condición termine, el grado de cansancio debe ser reducido a 0.\nLa condición de exhausto esta divida en 6 niveles\n\n\u2022 Desventaja en las pruebas de habilidad.\n\n\u2022 Movimiento reducido a la mitad.\n\n\u2022 Desventaja en tiradas de ataque y de salvación.\n\n\u2022 Pts de golpe totales reducidos a la mitad.\n\n\u2022 Movimiento reducido a 0.\n\n\u2022 Muerte.\n\nTerminar un descanso prolongado reduce en 1 grado la condición de exhausto de la criatura, siempre y cuando esta haya comido y bebido algo.")
                          :
                          Text("Pulsa para ver la descripción", style: TextStyle(fontStyle: FontStyle.italic),),
                        trailing: Switch(value: exhausto, onChanged: (value) async{
                          /*final res = await editarEstado(id, "inconsciente", value);
                          if(res["succesful"] != null) {*/
                            setState(() {
                              exhausto = value;
                              if(exhausto){
                                numEstados++;
                              }else {
                                numEstados--;
                              }
                            });
                          /*}else {
                            showToast(res["error"]);
                          }*/
                        },),
                        onTap: () {
                          setState(() {
                            subtitleExhausto = !subtitleExhausto;
                          });
                        },
                      ),
                      Divider(),
                    ],
                  )
              ),
          ),
        appBar: AppBar(
          leading: BackButton(),
          backgroundColor: Colors.red[400],
          title: Text(nombre),
        ),
        backgroundColor: Colors.deepPurple[800],
        body: 
          Column(
            children: [
              Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                height: /*numEstados > 11 ? */size.height * 0.3, /*: size.height * 0.25,*/ //100, 0.3?
                width: size.width,
                color: Colors.deepPurple[900],
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                          Stack(
                          clipBehavior: Clip.none,
                          alignment: AlignmentDirectional.centerStart,
                          children: [
                            Container(width: size.width * 0.94, height: size.height * 0.25,),
                            Positioned(
                              left: size.width * 0.165,
                              top: size.height * 0.040,
                              child: 
                                Container(
                                  width: size.width * 0.4,
                                  child: 
                                    Text(nombre, style: TextStyle(color: Colors.white), overflow: TextOverflow.fade)
                                )
                            ),
                            Positioned(
                              left: size.width * 0.165,
                              top: size.height * 0.065,
                              child: 
                                Container(
                                  width: size.width * 0.4,
                                  child: 
                                    Text(clase+" "+nivel, style: TextStyle(color: Colors.white), overflow: TextOverflow.fade,)
                                )
                            ),
                            Positioned(
                              left: size.width * 0.15,
                              child:
                                LinearPercentIndicator(
                                  width: 170.0,
                                  lineHeight: 30.0,
                                  percent: percent,
                                  center: 
                                    BorderedText(
                                      strokeWidth: 1.0, 
                                      child: 
                                        Text(
                                          "${currentHP}"+"/"+"${maxHP}", 
                                          style: 
                                            TextStyle(
                                              fontWeight: 
                                                FontWeight.bold, 
                                              color: 
                                                Colors.white
                                            ),
                                        )
                                    ),
                                  backgroundColor: Colors.white30,
                                  progressColor: Colors.red,
                                ),
                            ),
                            CircleAvatar(
                              radius: 30,
                              backgroundImage: widget.imagen.image,
                            ),
                            Positioned(
                            left: size.width * 0.58,//205,
                            child: 
                              Image.asset(
                                "assets/icons/shield2.png",
                                width: 70,
                                height: 70,
                              )
                            ),
                            Positioned(
                              left: size.width * 0.645,//229,
                              child:
                                Text(clase_armadura.toString(), style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold,)) 
                            ),
                            Positioned(
                              left: size.width * 0.78,
                              child:
                                Column(
                                  children: [
                                    ElevatedButton(
                                      child: 
                                        Text(
                                          "Estados", 
                                          style: 
                                            TextStyle(
                                              fontSize: 12
                                            ),
                                        ),
                                      style: 
                                        ElevatedButton.styleFrom(
                                          primary:
                                            Colors.amber[700],
                                          fixedSize: 
                                            Size(15, 15), 
                                          padding: 
                                            EdgeInsets.all(2)),
                                      onPressed: () {
                                        setState(() {
                                          _scaffoldKey.currentState.openDrawer();
                                        });
                                      },
                                    ),
                                    ElevatedButton(
                                      child: 
                                        Text(
                                          "Salud", 
                                          style: 
                                            TextStyle(
                                              fontSize: 12
                                            ),
                                        ),
                                      style: 
                                        ElevatedButton.styleFrom(
                                          primary:
                                            Colors.red, 
                                          fixedSize: 
                                            Size(15, 15), 
                                          padding: 
                                            EdgeInsets.all(2)),
                                      onPressed: () async{
                                        _dialogCall(context);

                                        /*if(dataSalud?.isNotEmpty ?? false){
                                          currentHP = dataSalud["vida"];
                                          temporalHP = dataSalud["temp"];
                                        }*/
                                      },
                                    )
                                  ],
                                )
                            ),
                            Positioned(
                              top: size.height * 0.2,
                              child: 
                                Container(
                                  width: size.width * 0.7,
                                  child: 
                                    Wrap(
                                      runSpacing: 5,
                                      children: [
                                        Visibility(
                                          visible: envenenado == null? false : envenenado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/poison.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: cegado == null? false : cegado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/blind.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: encantado == null? false : encantado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/enchanted-mirror.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: ensordecido == null? false : ensordecido,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/deafness.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: asustado == null? false : asustado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/fear.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: agarrado == null? false : agarrado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/self-restraint.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: incapacitado == null? false : incapacitado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/tied.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: invisible == null? false : invisible,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/invisible.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: paralizado == null? false : paralizado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/lightning.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: petrificado == null? false : petrificado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/sculpture(1).png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: tumbado == null? false : tumbado,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/yoga.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: restringido == null? false : restringido,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/rope.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: aturdido == null? false : aturdido,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/dizziness.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: inconsciente == null? false : inconsciente,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/dead.png"),
                                            )
                                        ),
                                        Visibility(
                                          visible: exhausto == null? false : exhausto,
                                          child: 
                                            Container(
                                              height: 22,
                                              width: 22,
                                              child: 
                                                Image.asset("assets/icons/tiredness.png"),
                                            )
                                        ),
                                      ],
                                    ),
                                )
                            )
                          ],
                        ),
                      ],
                    ),
                  ]
                )
              ),
              SizedBox(height: size.height * 0.02),
              Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                height: size.height * 0.48,
                child:
                Column(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                      height: (size.height * 0.45)*0.40,
                      child:
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(
                            onPressed: () async{
                              estados = {"envenenado" : envenenado,
                                "cegado" : cegado,
                                "paralizado" : paralizado,
                                "petrificado" : petrificado,
                                "tumbado" : tumbado,
                                "agarrado" : agarrado,
                                "encantado" : encantado,
                                "ensordecido" : ensordecido,
                                "asustado" : asustado,
                                "incapacitado" : incapacitado,
                                "invisible" : invisible,
                                "restringido" : restringido,
                                "aturdido" : aturdido,
                                "inconsciente" : inconsciente,
                                "exhausto" : exhausto,
                              };
                              final res = await editEstados(id, estados);
                              if(res["error"] != null){
                                showToast(res["error"]);
                              }else {
                                showToast(res["succesful"]);
                                Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => DiarioScreen(id)
                                ),
                              );
                              } 
                              
                            }, 
                            iconSize: 90,
                            icon: 
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),//or 15.0
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  height: 90.0,
                                  width: 90.0,
                                  color: Colors.white24,
                                  child: Image.asset("assets/icons/diary.png"),
                                ),
                              ),
                          ),
                          IconButton(
                            onPressed: () async{
                              estados = {
                                "envenenado" : envenenado,
                                "cegado" : cegado,
                                "paralizado" : paralizado,
                                "petrificado" : petrificado,
                                "tumbado" : tumbado,
                                "agarrado" : agarrado,
                                "encantado" : encantado,
                                "ensordecido" : ensordecido,
                                "asustado" : asustado,
                                "incapacitado" : incapacitado,
                                "invisible" : invisible,
                                "restringido" : restringido,
                                "aturdido" : aturdido,
                                "inconsciente" : inconsciente,
                                "exhausto" : exhausto,
                              };
                              final res = await editEstados(id, estados);
                              if(res["error"] != null){
                                showToast(res["error"]);
                              }else {
                                showToast(res["succesful"]);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => InventarioScreen(id)
                                  ),
                                );
                              }
                            }, 
                            iconSize: 90,
                            icon: 
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20.0),//or 15.0
                                child: Container(
                                  padding: EdgeInsets.all(10),
                                  height: 90.0,
                                  width: 90.0,
                                  color: Colors.white24,
                                  child: Image.asset("assets/icons/bag.png"),
                                ),
                              ),
                          ),
                        ],)
                      ),
                      Container(
                        height: size.height * 0.1,
                        child: 
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Visibility(
                                visible: dadosVisible,
                                child:
                                  FloatingActionButton(
                                    heroTag: "btn4",
                                    backgroundColor: Colors.black,
                                    onPressed: (){
                                        tirarDados(4);
                                    }, 
                                    child: CircleAvatar(child: Image.asset("assets/icons/dice/d4.png"))
                                  )
                                /*CircleAvatar(
                                  backgroundColor: Colors.black,
                                  child:  
                                  IconButton(
                                    icon: Image.asset("assets/icons/dice/d4.png"),
                                    onPressed: (){
                                      tirarDados(4);
                                    },
                                  ))*/
                              ),
                              Visibility(
                                visible: dadosVisible,
                                child:
                                  FloatingActionButton(
                                      heroTag: "btn6",
                                      backgroundColor: Colors.black,
                                      onPressed: (){
                                          tirarDados(6);
                                      }, 
                                      child: CircleAvatar(child: Image.asset("assets/icons/dice/d6.png"))
                                  ) 
                                  /*IconButton(
                                    icon: Image.asset("assets/icons/dice/d6.png"),
                                    onPressed: (){
                                      tirarDados(6);
                                    },
                                  )*/
                              ),
                              Visibility(
                                visible: dadosVisible,
                                child: 
                                  FloatingActionButton(
                                    heroTag: "btn8",
                                    backgroundColor: Colors.black,
                                    onPressed: (){
                                        tirarDados(8);
                                    }, 
                                    child: CircleAvatar(child: Image.asset("assets/icons/dice/d8.png"))
                                  )
                                  /*IconButton(
                                    icon: Image.asset("assets/icons/dice/d8.png"),
                                    onPressed: (){
                                      tirarDados(8);
                                    },
                                  )*/
                              ),
                            ],
                          ),
                      ),
                      Visibility(
                        visible: dadosOpen,
                        child: SizedBox(height: size.height * 0.01)
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: dadosOpen? 40 : 60,
                            width: dadosOpen? 40 : 60,
                            child:
                              FloatingActionButton(
                                heroTag: "btn1",
                                backgroundColor: dadosOpen? Colors.red : Colors.black,
                                onPressed: (){
                                  setState(() {
                                    if(dadosOpen){
                                      dadosOpen = false;
                                      dadosVisible = false;
                                    }else {
                                      dadosOpen = true;
                                      dadosVisible = true;
                                    }
                                  });
                                }, 
                                child: dadosOpen? Icon(Icons.close) : CircleAvatar(child: Image.asset("assets/icons/dice/rolling-dices.png"))
                              )
                          )
                        ]
                      ),
                      Visibility(
                        visible: dadosOpen,
                        child: SizedBox(height: size.height * 0.01),
                      ),
                      Container(
                        height: size.height * 0.1,
                        child:  
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Visibility(
                                visible: dadosVisible,
                                child: 
                                  FloatingActionButton(
                                    heroTag: "btn10",
                                    backgroundColor: Colors.black,
                                    onPressed: (){
                                        tirarDados(10);
                                    }, 
                                    child: CircleAvatar(child: Image.asset("assets/icons/dice/d10.png"))
                                  )
                                  /*IconButton(
                                    icon: Image.asset("assets/icons/dice/d10.png"),
                                    onPressed: (){
                                      tirarDados(10);
                                    },
                                  )*/

                              ),
                              Visibility(
                                visible: dadosVisible,
                                child: 
                                  FloatingActionButton(
                                    heroTag: "btn12",
                                    backgroundColor: Colors.black,
                                    onPressed: (){
                                        tirarDados(12);
                                    }, 
                                    child: CircleAvatar(child: Image.asset("assets/icons/dice/d12.png"))
                                  )
                                  /*IconButton(
                                    icon: Image.asset("assets/icons/dice/d12.png"),
                                    onPressed: (){
                                      tirarDados(12);
                                    },
                                  )*/
                              ),
                              Visibility(
                                visible: dadosVisible,
                                child: 
                                  FloatingActionButton(
                                    heroTag: "btn20",
                                    backgroundColor: Colors.black,
                                    onPressed: (){
                                        tirarDados(20);
                                    }, 
                                    child: CircleAvatar(child: Image.asset("assets/icons/dice/d20.png"))
                                  )
                                  /*IconButton(
                                    icon: Image.asset("assets/icons/dice/d20.png"),
                                    onPressed: (){
                                      tirarDados(20);
                                    },
                                  )*/
                              ),
                            ],
                          ) 
                        ,
                      )
                  ],
                )
              ),
              Expanded(
                child:
                  Container(
                    color: Colors.black38 ,
                    //height: size.height * 0.105, 
                    child: 
                      Row( 
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children:[
                          GestureDetector(
                            onLongPress: () {
                              int value = int.parse(numDadosCtrl.text.replaceAll("d", ""));
                              if(value > 1){
                                setState(() {
                                  numDadosCtrl.text = "1d";
                                });
                              }
                            },
                            onTap: (){
                              int value = int.parse(numDadosCtrl.text.replaceAll("d", ""));
                              if(value > 1){
                                setState(() {
                                  numDadosCtrl.text = (value - 1).toString()+"d";
                                });
                              }
                            },
                            child:
                              Icon(Icons.remove_circle_outline, color: Colors.white, size: 35),
                          ),
                          Container(
                            width: size.width * 0.2,
                            child: 
                              TextField(
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                                readOnly: true,
                                decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white, width: 2.0),
                                      borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                                controller: numDadosCtrl,
                              )
                          ),
                          GestureDetector(
                            onLongPress: () {
                              int value = int.parse(numDadosCtrl.text.replaceAll("d", ""));
                              if(value < 100){
                                setState(() {
                                  numDadosCtrl.text = "100d";
                                });
                              }
                            },
                            onTap: (){
                              int value = int.parse(numDadosCtrl.text.replaceAll("d", ""));
                              if(value < 100){
                                setState(() {
                                  numDadosCtrl.text = (value + 1).toString()+"d";
                                });
                              }
                            },
                            child: Icon(Icons.add_circle_outline, color: Colors.white, size: 35),
                          ),
                          SizedBox(width: size.width * 0.05),
                          GestureDetector(
                            onLongPress: () {
                              int value = int.parse(bonusDadosCtrl.text.replaceAll("+", ""));
                              if(value > 0){
                                setState(() {
                                  bonusDadosCtrl.text = "+0";
                                });
                              }
                            },
                            onTap: (){
                              int value = int.parse(bonusDadosCtrl.text.replaceAll("+", ""));
                              if(value > 0){
                                setState(() {
                                  bonusDadosCtrl.text = "+"+(value - 1).toString();
                                });
                              }
                            },
                            child:
                              Icon(Icons.remove_circle_outline, color: Colors.white, size: 35,),
                          ),
                          Container(
                            width: size.width * 0.2,
                            child: 
                              TextField(
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white),
                                readOnly: true,
                                decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.white, width: 2.0),
                                      borderRadius: BorderRadius.circular(10.0),
                                  ),
                                ),
                                controller: bonusDadosCtrl,
                              )
                          ),
                          GestureDetector(
                            onLongPress: () {
                              int value = int.parse(bonusDadosCtrl.text.replaceAll("+", ""));
                              if(value < 100){
                                setState(() {
                                  bonusDadosCtrl.text = "+100";
                                });
                              }
                            },
                            onTap: (){
                              int value = int.parse(bonusDadosCtrl.text.replaceAll("+", ""));
                              if(value < 100){
                                setState(() {
                                  bonusDadosCtrl.text = "+"+(value + 1).toString();
                                });
                              }
                            },
                            child: Icon(Icons.add_circle_outline, color: Colors.white, size: 35),
                          ),
                        ]
                      )
                  )
              )
          ],)
      )
    );
  }
}

class MyDialogContent extends StatefulWidget {

  final currentHP;
  final temporalHP;
  final maxHP;
  final id;

  MyDialogContent(this.currentHP, this.temporalHP, this.maxHP, this.id, {
    Key key,
  }): super(key: key);

  @override
  _MyDialogContentState createState() => new _MyDialogContentState();
}

class _MyDialogContentState extends State<MyDialogContent> {
  TextEditingController danyo = TextEditingController();
  TextEditingController temporales = TextEditingController();
  TextEditingController curacion = TextEditingController();
  final Map<String, int> parametros = {
    "temp" : 0,
    "vida" : 0
  };
  
  final _formKey = GlobalKey<FormState>();

  FocusNode focusDanyo = FocusNode();
  FocusNode focusCurar = FocusNode();

  String hintDanyo = "0";
  String hintCurar = "0";

  String tempHP;
  int vida;

  void initState() {
    super.initState();
    temporales.text = widget.temporalHP.toString();

    parametros ["temp"] = widget.temporalHP;
    parametros ["vida"] = widget.currentHP;
    tempHP = widget.temporalHP.toString();
    vida = widget.currentHP;

    focusDanyo.addListener(() {
      if (focusDanyo.hasFocus) {
        hintDanyo = '';
      } else {
        danyo.clear();
        temporales.text = tempHP;
        hintDanyo = '0';
      }
      setState(() {});
    });

    focusCurar.addListener(() {
      if (focusCurar.hasFocus) {
        hintCurar = '';
      } else {
        curacion.clear();
        temporales.text = tempHP;
        hintCurar = '0';
      }
      setState(() {});
    });
      
  }

  Widget build(BuildContext context){
    Size size = MediaQuery.of(context).size;
    return AlertDialog(
      contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
      insetPadding: EdgeInsets.all(10),
      content: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Salud", style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: size.height * 0.02),
                    Row(
                      children: [
                        Expanded(
                          child: 
                            Column(
                              children: [
                                Text("Daño", style: TextStyle(fontSize: 14, color: Colors.red)),
                                SizedBox(height: size.height * 0.02),
                                TextFormField(
                                  focusNode: focusDanyo,
                                  onChanged: (value) {
                                    setState(() {
                                      if(value == ""){
                                        /*curacion.text = "";
                                        temporales.text = tempHP;
                                        danyo.clear();*/
                                      }else {
                                        temporales.text = tempHP;
                                        //curacion.text = "0";
                                        if(int.parse(danyo.text) < int.parse(temporales.text)){
                                          temporales.text = (int.parse(temporales.text) - int.parse(danyo.text)).toString();
                                        }else {
                                          temporales.text ="0";
                                        }
                                      }
                                    });
                                  },
                                  textAlign: TextAlign.center, 
                                  controller: danyo, 
                                  keyboardType: TextInputType.number, 
                                  decoration: InputDecoration(
                                    hintText: hintDanyo,
                                    hintStyle: TextStyle(color: Colors.red),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.red,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)
                                    )
                                  ),
                                  style: TextStyle(color: Colors.red),
                                )
                              ],
                            )
                        ),
                        SizedBox(width: size.width * 0.04),
                        Expanded(
                          child: 
                            Column(
                              children: [
                                Text("Temporales", style: TextStyle(fontSize: 14, color: Colors.purple)),
                                SizedBox(height: size.height * 0.02),
                                TextFormField(
                                  onChanged: (value){ 
                                    setState(() {
                                      danyo.text = "0";
                                      curacion.text = "0";
                                    });
                                  },
                                  textAlign: TextAlign.center, 
                                  controller: temporales, 
                                  keyboardType: TextInputType.number, 
                                  decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.purple,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.purple,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)
                                    )
                                  ),
                                  style: TextStyle(color: Colors.purple),
                                )
                              ],
                            )
                        ),
                        SizedBox(width: size.width * 0.04),
                        Expanded(
                          child: 
                            Column(
                              children: [
                                Text("Curación", style: TextStyle(fontSize: 14, color: Colors.green)),
                                SizedBox(height: size.height * 0.02),
                                TextFormField(
                                  focusNode: focusCurar,
                                  onChanged: (value){
                                    setState(() {
                                      /*if(value == ""){
                                        curacion.clear();
                                      }
                                      danyo.text = "";
                                      temporales.text = tempHP;*/
                                      
                                    });
                                  },
                                  textAlign: TextAlign.center, 
                                  controller: curacion, 
                                  keyboardType: TextInputType.number, 
                                  decoration: InputDecoration(
                                    hintText: hintCurar,
                                    hintStyle: TextStyle(color: Colors.green),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.green,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Colors.green,
                                        width: 2.0,
                                      ),
                                      borderRadius: BorderRadius.circular(10)
                                    )
                                  ),
                                  style: TextStyle(color: Colors.green),
                                ),
                              ],
                            )
                        )
                      ],
                    ),
                    SizedBox(height: size.height * 0.05,),
                    ElevatedButton(
                      child: 
                        Text(
                          "Guardar", 
                        ),
                      style: 
                        ElevatedButton.styleFrom(
                          primary:
                            Colors.red,
                        ),
                      onPressed: () async{
                        if(curacion.text != "" && curacion.text != "0"){

                          if(vida == widget.maxHP) {
                            Navigator.pop(context);
                          }else {
                            vida = vida + int.parse(curacion.text);

                            if(vida > widget.maxHP){
                              vida = widget.maxHP;
                            }
                            parametros["vida"] = vida;
                            final res = await curacionPJ(widget.id, vida);
                            if(res["succesful"] != null){
                              Navigator.pop(context, parametros);

                            }else {
                              showToast(res["error"]);
                            }
                          }
                        }else if(temporales.text != tempHP && (danyo.text == "" || danyo.text == "0")){
                          parametros["temp"] = int.parse(temporales.text);

                          final res = await addTemp(widget.id, parametros["temp"]);
                          
                          if(res["succesful"] != null){
                            //Navigator.pop(context, parametros);
                            Navigator.pop(context);
                          }else {
                            showToast(res["error"]);
                          }
                        }else if(danyo.text != "" && danyo.text != "0"){
                          int danyoTotal = int.parse(danyo.text) - int.parse(tempHP);
                          if(danyoTotal < 0) {
                            danyoTotal = 0;
                          }

                          danyo.text = danyoTotal.toString();

                          vida = vida - danyoTotal; 

                          parametros["temp"] = int.parse(temporales.text);
                          parametros["vida"] = vida;

                          final res = await vidayTemp(widget.id, vida, parametros["temp"]);
                          
                          if(res["succesful"] != null){
                            //Navigator.pop(context, parametros);
                            Navigator.pop(context);
                          }else {
                            showToast(res["error"]);
                          }
                        }else {
                          Navigator.pop(context);
                        }
                      }
                    ),
                ]
              )
            )
          ]
        )
      )
    );
  }
}

