import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:proyecto_final/components/show_toast.dart';
import 'package:proyecto_final/data/diario_service.dart';
import 'package:proyecto_final/data/listaPJs_service.dart';
import 'package:proyecto_final/data/login_service.dart';
import 'package:proyecto_final/screens/pj_screen.dart';
import 'package:path_provider/path_provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> { 

  //TODO mirar lo de las imagenes. Donde y como guardarlas


  List<ItemPersonaje> nuevosPersonajes = List.empty(growable: true);
  int count=0;
  String userName;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    cargarVisual();
  }

  bool intToBool(int num){
    return num == 0 ? false : true;
  }

  void cargarVisual() async{
    userName = await getUserValue(); 
    dynamic res = await getDatosPJ(userName);
    BuildContext context;
    //Size size = MediaQuery.of(context).size;
    Image widgetImg;

    if(res != null || res != ""){
      var id;
      var nombre;
      var campanya;
      var clase;
      var nivel;
      var maxHP;
      var currentHP;
      var temporalHP; 
      var clase_armadura; 

      /*var envenenado;
      var cegado;
      var paralizado; 
      var petrificado; 
      var tumbado;
      var agarrado;
      var encantado; 
      var ensordecido; 
      var asustado;
      var incapacitado; 
      var invisible;
      var restringido;
      var aturdido; 
      var inconsciente; 
      var exhausto; 
      var peso_equipo; 
      var p_oro; 
      var p_plata; 
      var p_cobre; 
      var p_platino; 
      var p_electrum;*/ 

      for(int i = 0; i < res["succesful"]["nombres"].length; i++){
        Map<String, dynamic> pjStats;

        id = res["succesful"]["ides"][i];
        nombre = res["succesful"]["nombres"][i];

        if(res["succesful"]["campanyas"][i] == null){
          campanya = " ";
        }else {
          campanya = res["succesful"]["campanyas"][i];
        }
        if(res["succesful"]["campanyas"][i] == null){
          widgetImg = Image.asset("assets/icons/warrior.png");
        }else {
          widgetImg = Image.asset("assets/icons/warrior.png"); //TODO cambiar esto por la imagen almacenada
        }

        clase = res["succesful"]["clases"][i];
        nivel = res["succesful"]["niveles"][i].toString();
        maxHP = res["succesful"]["vidas_max"][i];
        currentHP = res["succesful"]["vidas_actuales"][i];
        temporalHP = res["succesful"]["tempHPs"][i];
        clase_armadura = res["succesful"]["clases_armaduras"][i];
        /*envenenado = intToBool(res["succesful"]["envenenados"][i]);
        cegado = intToBool(res["succesful"]["cegados"][i]);
        paralizado = intToBool(res["succesful"]["paralizados"][i]);
        petrificado = intToBool(res["succesful"]["petrificados"][i]);
        tumbado = intToBool(res["succesful"]["tumbados"][i]);
        agarrado = intToBool(res["succesful"]["agarrados"][i]);
        encantado = intToBool(res["succesful"]["encantados"][i]);
        ensordecido = intToBool(res["succesful"]["ensordecidos"][i]);
        asustado = intToBool(res["succesful"]["asustados"][i]);
        incapacitado = intToBool(res["succesful"]["incapacitados"][i]);
        invisible = intToBool(res["succesful"]["invisibles"][i]);
        restringido = intToBool(res["succesful"]["restringidos"][i]);
        aturdido = intToBool(res["succesful"]["aturdidos"][i]);
        inconsciente = intToBool(res["succesful"]["inconscientes"][i]);
        exhausto = intToBool(res["succesful"]["exhaustos"][i]);
        peso_equipo = res["succesful"]["pesos_equipo"][i];
        p_oro = res["succesful"]["pzs_oro"][i];
        p_plata = res["succesful"]["pzs_plata"][i];
        p_cobre = res["succesful"]["pzs_cobre"][i];
        p_platino = res["succesful"]["pzs_platino"][i];
        p_electrum = res["succesful"]["pzs_electrum"][i];*/

        pjStats = {
          "id" : id,
          "nombre" : nombre,
          "clase" : clase,
          "nivel" : nivel,
          "maxHP" : maxHP,
          "currentHP" : currentHP,
          "temporalHP" : temporalHP,
          "clase_armadura" : clase_armadura,
         /* "envenenado" : envenenado,
          "cegado" : cegado,
          "paralizado" : paralizado,
          "petrificado" : petrificado,
          "tumbado" : tumbado,
          "agarrado" : agarrado,
          "encantado" : encantado,
          "ensordecido" : ensordecido,
          "asustado" : asustado,
          "incapacitado" : incapacitado,
          "invisible" : invisible,
          "restringido" : restringido,
          "aturdido" : aturdido,
          "inconsciente" : inconsciente,
          "exhausto" : exhausto,
          "peso_equipo" : peso_equipo,
          "p_oro" : p_oro,
          "p_plata" : p_plata,
          "p_cobre" : p_cobre,
          "p_platino" : p_platino,
          "p_electrum" : p_electrum,*/
        };

        setState(() {
          nuevosPersonajes.add(ItemPersonaje(widgetImg, /*size,*/ nombre, campanya, clase, nivel, pjStats, i, nuevosPersonajes, removeItem, refresh, index: id));
        });
      }
    }
  }

  Future<void> _dialogCall(BuildContext context, Function state) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return MyDialogContent(nuevosPersonajes, userName, state, removeItem, "anyadir", null, null, refresh);
        }).then(refresh);
  }

  void removeItem(index, lista) {
    setState(() {
      lista.removeWhere((item) => item.index == index);
    });
  }

  void state() {
    setState((){});
  }

  FutureOr refresh(dynamic value){
    nuevosPersonajes.clear();
    cargarVisual();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[400],
        title: const Text('Adventurer\'s Kit'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
        child: 
          Column(
            children: [
              Row(
                children: [
                  Text("Lista de Personajes"),
                  IconButton(
                    icon: Icon(
                      Icons.add_circle_outline, 
                      color: Colors.green,), 
                    onPressed: (){
                      setState(() {
                        count = nuevosPersonajes.length;
                        _dialogCall(context, state);
                      });
                    }
                  )
              ],),
              nuevosPersonajes.isNotEmpty 
                ? ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true, 
                    children: <Widget>[...nuevosPersonajes],
                  ) 
                : Text("No hay personajes creados",
                    style: 
                      TextStyle(
                        fontStyle: FontStyle.italic,
                        color: Colors.blueGrey[300]
                      )     
                  )
          ],)
      ),
    );
  }
}

class ItemPersonaje extends StatefulWidget {
  final int count;
  final int index;
  final Function(dynamic, dynamic) removeItem;
  final Function refresh;
   String nombre;
  final Image imagen;
   String campanya;
   String clase;
   String nivel;
   Map<String, dynamic> pjStats;
  //final Size size;
  final List<ItemPersonaje> lista;

   ItemPersonaje(this.imagen, /*this.size,*/ this.nombre, this.campanya, this.clase, this.nivel, this.pjStats, this.count, this.lista, this.removeItem, this.refresh, {Key key, @required this.index})
      : super(key: key);

  @override
  ItemPersonajeState createState() => new ItemPersonajeState();
}

class ItemPersonajeState extends State<ItemPersonaje>{
  @override
  Widget build(BuildContext context){
    Size size = MediaQuery.of(context).size;
    return 
      InkWell(
        onTap: (){
          var pjStats2 = widget.pjStats;
          pjStats2.remove("maxHP");
          pjStats2.remove("currentHP");
          pjStats2.remove("temporalHP");

           Route route = MaterialPageRoute(builder: (context) => PJScreen(widget.imagen, pjStats2));
          Navigator.push(context, route).then(widget.refresh);
          /*Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PJScreen(widget.imagen, pjStats2)
            ),
          );*/
        },
        child: 
          Card(
            elevation: 5,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child:
              Padding(
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child:
                IntrinsicHeight (child: 
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start, //change here don't //worked
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircleAvatar(
                        radius: 30,
                        backgroundImage: widget.imagen.image,
                      ),
                      SizedBox(width: /*widget.*/size.width * 0.03),
                      Expanded(child: 
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.nombre, style: TextStyle(fontWeight: FontWeight.bold)),
                            Text(widget.clase+" "+widget.nivel),
                            Text(widget.campanya, style: TextStyle(color: Colors.grey ), maxLines: 2, overflow: TextOverflow.ellipsis,)
                          ],
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: 
                          IconButton(
                            icon: Icon(Icons.edit, color: Colors.grey), 
                            onPressed: () async{
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  var userName = getUserValue();
                                  return MyDialogContent(widget.lista, userName, setState, widget.removeItem, "editar", widget.pjStats, widget.index, widget.refresh);
                                }
                              ).then(widget.refresh);
                            },
                          )
                      ,),
                      Align(
                        alignment: Alignment.centerRight,
                        child: 
                          VerticalDivider(color: Colors.grey[400],)
                      ,),
                      Align(
                        alignment: Alignment.centerRight,
                        child: 
                          IconButton(
                            icon: Icon(Icons.delete, color: Colors.red), 
                            onPressed: (){
                              setState(() {
                                showDialog(context: context, builder: (BuildContext builder){
                                  return 
                                    AlertDialog(
                                      contentPadding: EdgeInsets.fromLTRB(20, 20, 20, 20),
                                      insetPadding: EdgeInsets.all(10),
                                      title: Text("Estás a punto de borrar a ${widget.nombre} y todo su contenido. ¿Estás seguro que quieres continuar?"), 
                                      content: Container(height: size.height * 0.02),
                                      actionsAlignment: MainAxisAlignment.center,
                                      actions: 
                                        <Widget>[
                                          ElevatedButton(
                                              onPressed: () async{
                                                final dynamic res = await borrarPJ(widget.index);
                                                if(res["succesful"] != null){
                                                  widget.removeItem(widget.index, widget.lista);
                                                  Navigator.pop(context);
                                                  showToast(res['succesful']);
                                                }else{
                                                  showToast(res['error']);
                                                }
                                              }, 
                                              child: Text("Eliminar")
                                            ),
                                            ElevatedButton(
                                              onPressed: () async{
                                                Navigator.pop(context);
                                              }, 
                                              child: Text("Cancelar")
                                            )
                                        ],     
                                    );
                                });
                              });
                            },
                          )
                      ,)
                  ],)
                ,)
              ,)
          )
      );
  }
}

class MyDialogContent extends StatefulWidget {
  final nuevosPersonajes;
  final userName;
  final tipo;
  final idPJ;
  final Map<String, dynamic> pjStats;
  final Function state;
  final Function removeItem;
  final Function refresh;
  MyDialogContent(this.nuevosPersonajes, this.userName, this.state, this.removeItem, this.tipo, this.pjStats, this.idPJ, this.refresh, {
    Key key,
  }): super(key: key);

  @override
  _MyDialogContentState createState() => new _MyDialogContentState();
}

class _MyDialogContentState extends State<MyDialogContent> {
    final _formKey = GlobalKey<FormState>();
    TextEditingController nombre = TextEditingController();
    TextEditingController clase = TextEditingController();
    TextEditingController nivel = TextEditingController();
    TextEditingController campanya = TextEditingController();
    TextEditingController vidaMax = TextEditingController();
    TextEditingController clase_armadura = TextEditingController();

    initState(){
      initControllers();
    }

    initControllers(){
      if(widget.tipo == "editar"){
        nombre.text = widget.pjStats["nombre"];
        clase.text = widget.pjStats["clase"];
        nivel.text = widget.pjStats["nivel"];
        campanya.text = widget.pjStats["campanya"];
        vidaMax.text = widget.pjStats["maxHP"].toString();
        clase_armadura.text = widget.pjStats["clase_armadura"].toString();

        nombre.selection = TextSelection.fromPosition(TextPosition(offset: nombre.text.length));
        clase.selection = TextSelection.fromPosition(TextPosition(offset: clase.text.length));
        nivel.selection = TextSelection.fromPosition(TextPosition(offset: nivel.text.length));
        campanya.selection = TextSelection.fromPosition(TextPosition(offset: campanya.text.length));
        vidaMax.selection = TextSelection.fromPosition(TextPosition(offset: vidaMax.text.length));
        clase_armadura.selection = TextSelection.fromPosition(TextPosition(offset: clase_armadura.text.length));
      }
    }

    Map<String, dynamic> pjStats;
    
    File _image;
    String rutaImgSelected;
    bool _imgSelected = false;

    final picker = ImagePicker();
    Image widgetImg = Image.asset("assets/icons/warrior.png");

    

    Future getImage() async {
      final pickedFile = await picker.pickImage(source: ImageSource.gallery);
      Directory storageDir = await getApplicationDocumentsDirectory();
      String storageDirPath = storageDir.path;

      
        if (pickedFile != null) {
          _image = File(pickedFile.path);
          /*final name = basename(_image.path);
          final imageFile = File('${storageDir.path}/$name');
          final newImage = await File(pickedFile.path).copy(imageFile.path);*/
          setState(() {
            widgetImg = Image.file(_image);
          });
          _imgSelected=true;
        } else {
          _imgSelected=false;
        }
      
    }

    void addItem(imagen, nombre, /*size,*/ lista, clase, nivel, pjStats, id, campanya, count){
      lista.add(ItemPersonaje(imagen, /*size,*/ nombre, campanya, clase, nivel, pjStats, count, lista, widget.removeItem, widget.refresh, index:id));  
    }
    
  Widget build(BuildContext context){
    //initControllers();
    Size size = MediaQuery.of(context).size;
    Image widgetImg = _imgSelected  ? Image.file(_image, fit: BoxFit.cover) : Image.asset("assets/icons/warrior.png");
    return AlertDialog(
      contentPadding: EdgeInsets.fromLTRB(40, 40, 40, 20),
      content: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text("Foto del PJ"),
                  Stack(
                    children: [
                      Container(
                        width: 100,
                        height: 100,
                        child: 
                          CircleAvatar(
                            radius: 40,
                            backgroundImage: widgetImg.image,
                          )
                      ),
                      Positioned(
                        right: 0.0,
                        bottom: 0.0,
                        child: 
                          Container(
                            width: 40,
                            height: 40,
                            child: 
                              FloatingActionButton(
                                child: Icon(Icons.camera_alt),
                                backgroundColor: Colors.green.shade800,
                                onPressed: () async{
                                  await getImage();
                                  if(!_imgSelected){
                                    Fluttertoast.showToast(msg: "No se pudo seleccionar la imagen");
                                  }
                                }, 
                              )
                          )
                      ),
                    ],
                  ),
                  SizedBox(height: size.height * 0.03,),
                  Text("Nombre del PJ"),
                  TextFormField(
                    textAlign: TextAlign.center,
                    /*decoration:
                        InputDecoration(hintText: "Nombre del PJ"),*/
                    controller: nombre,
                    validator: (value) {
                      if (value == null || value == "") {
                        return "Campo obligatorio";
                      }
                    }
                  ),
                  //Divider(color: Colors.grey[200]),
                  SizedBox(height: size.height * 0.03,),
                  Text("Clase"),
                  TextFormField(
                      textAlign: TextAlign.center,
                      /*decoration:
                          InputDecoration(hintText: "Clase"),*/
                      controller: clase,
                      validator: (value) {
                        if (value == null || value == "") {
                          return "Campo obligatorio";
                        }
                      }),
                  //Divider(color: Colors.grey[200]),
                  SizedBox(height: size.height * 0.03,),
                  Text("Nivel"),
                  TextFormField(
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    controller: nivel,
                    /*decoration:
                      InputDecoration(hintText: "Nivel"),*/
                    validator: (value) {
                      if (value == null || value == "") {
                        return "Campo obligatorio";
                      }
                    },
                  ),
                  //Divider(color: Colors.grey[200]),
                  SizedBox(height: size.height * 0.03,),
                  Text("Campaña"),
                  TextFormField(
                    textAlign: TextAlign.center,
                    controller: campanya,
                    /*decoration:
                      InputDecoration(hintText: "Camapaña"),*/
                  ),
                  Divider(color: Colors.grey[200]),
                  Text("Vida máxima"),
                  TextFormField(
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    controller: vidaMax,
                    validator: (value) {
                      if (value == null || value == "") {
                        return "Campo obligatorio";
                      }
                      if(value.length > 3){
                        return "No más de 3 digitos";
                      }
                    },
                  ),
                  Divider(color: Colors.grey[200]),
                  Text("Clase de Armadura"),
                  TextFormField(
                    textAlign: TextAlign.center,
                    keyboardType: TextInputType.number,
                    controller: clase_armadura,
                    validator: (value) {
                      if (value == null || value == "") {
                        return "Campo obligatorio";
                      }
                      if(value.length > 2){
                        return "No más de 2 digitos";
                      }
                    },
                  ),
                  Divider(color: Colors.grey[200]),
                ]
              )
            ),
          ],
        ),
      ),
      actions: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              child: widget.tipo == "anyadir" ? Text("Crear PJ") : Text("Editar PJ"),
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  if(campanya.text == ""){
                    campanya.text = " ";
                  }
                  pjStats =
                    {
                      "id" : null,
                      "nombre" : nombre.text,
                      "clase" : clase.text,
                      "nivel" : int.parse(nivel.text),
                      "maxHP" : int.parse(vidaMax.text),
                      "currentHP" : int.parse(vidaMax.text),
                      "temporalHP" : 0,
                      "clase_armadura" : int.parse(clase_armadura.text),
                      "envenenado" : false,
                      "cegado" : false,
                      "paralizado" : false,
                      "petrificado" : false,
                      "tumbado" : false,
                      "agarrado" : false,
                      "encantado" : false,
                      "ensordecido" : false,
                      "asustado" : false,
                      "incapacitado" : false,
                      "invisible" : false,
                      "restringido" : false,
                      "aturdido" : false,
                      "inconsciente" : false,
                      "exhausto" : false,
                      "peso_equipo" : 0,
                      "p_oro" : 0,
                      "p_plata" : 0,
                      "p_cobre" : 0,
                      "p_platino" : 0,
                      "p_electrum" : 0,
                    };
                  /*var foto
                  if(_imgSelected){
                    foto = 
                  }*/
                  if(widget.tipo == "anyadir"){
                    final dynamic res = await newPJ(null, nombre.text, clase.text, nivel.text, campanya.text, vidaMax.text, clase_armadura.text, widget.userName);
                    if (res['succesful'] != null) {
                      var newId = res['succesful'];
                      pjStats["id"] = newId;
                      final respuesta = await crearPestanya(newId);
                      if(respuesta["error"] != null) {
                        showToast(respuesta["error"]);
                      }
                      Navigator.pop(context);
                      showToast("Personaje añadido correctamente");
                    } else {
                      showToast(res['error']);
                    }
                  }else if(widget.tipo == "editar"){
                    print("asdasds");
                    final dynamic res = await editarPJ(null, nombre.text, clase.text, nivel.text, campanya.text, vidaMax.text, clase_armadura.text, widget.idPJ);
                    print(res);
                    if (res['succesful'] != null) {
                      /*setState(() {
                        final pj = widget.nuevosPersonajes.firstWhere((item) => item.index == widget.idPJ);
                        print(pj.nombre);
                        pj.nombre = nombre.text; 
                        pj.clase = clase.text; 
                        pj.nivel = nivel.text; 
                        pj.campanya = campanya.text; 
                        pj.pjStats["maxHP"] = int.parse(vidaMax.text); 
                        pj.pjStats["clase_armadura"] = int.parse(clase_armadura.text); 
                        pj.pjStats["nombre"] = nombre.text;
                        pj.pjStats["clase"] = clase.text;
                        pj.pjStats["nivel"] = nivel.text;
                        pj.pjStats["campanya"] = campanya.text;
                      });
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (contex) => HomeScreen()
                        ),
                      );*/
                      Navigator.pop(context);
                      showToast(res['succesful']);
                    } else {
                      showToast(res['error']);
                    }
                  }
                }
              },
              style: ElevatedButton.styleFrom(
                primary: Colors.red,
                onPrimary: Colors.white, // foreground
              )
            ),
            SizedBox(width: size.width * 0.05,),
            ElevatedButton(
              onPressed: (){
                Navigator.pop(context);
              }, 
              child: Text("Cancelar"),
              style: ElevatedButton.styleFrom(
                primary: Colors.red,
                onPrimary: Colors.white, // foreground
              )
            )
          ],
        )
      ],
    );
  }
}