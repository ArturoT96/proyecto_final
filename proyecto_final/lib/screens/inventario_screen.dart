import 'dart:async';

import 'package:flutter/material.dart';
import 'package:proyecto_final/components/show_toast.dart';
import 'package:proyecto_final/data/inventario_service.dart';
import 'package:proyecto_final/screens/objetos_screen.dart';

class InventarioScreen extends StatefulWidget {

  final int idPj;

  InventarioScreen(this.idPj);
  
  @override
  InventarioScreenState createState() => new InventarioScreenState();
}

class InventarioScreenState extends State<InventarioScreen> {

  TextEditingController pptCntrl = TextEditingController();
  TextEditingController poCntrl = TextEditingController();
  TextEditingController peCntrl = TextEditingController();
  TextEditingController ppCntrl = TextEditingController();
  TextEditingController pcCntrl = TextEditingController();

  TextEditingController cantidad = TextEditingController();

  bool cantidadActivo;

  int peso = 0;

  final _formKey = GlobalKey<FormState>();

  List<DropdownMenuItem<dynamic>> monedas=[
    DropdownMenuItem(value: "ppt",child: Text("Platino")),
    DropdownMenuItem(value: "po",child: Text("Oro")), 
    DropdownMenuItem(value: "pe",child: Text("Electrum")),
    DropdownMenuItem(value: "pp",child: Text("Plata")), 
    DropdownMenuItem(value: "pc",child: Text("Cobre")),
  ];

  String tipoMoneda;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    cantidadActivo = false;

    datosMonedasPeso();

  }

  datosMonedasPeso() async{
    final res = await traerMonedasPeso(widget.idPj);
    if(res["error"] != null){
      showToast(res["error"]);
    }else {
      var respuesta = res["succesful"];

      setState(() {
        pptCntrl.text = respuesta["p_platino"].toString();
        poCntrl.text = respuesta["p_oro"].toString();
        peCntrl.text = respuesta["p_electrum"].toString();
        ppCntrl.text = respuesta["p_plata"].toString();
        pcCntrl.text = respuesta["p_cobre"].toString();

        peso = respuesta["peso_equipo"]; 
      });
    }
  }

  FutureOr refresh(dynamic value){
    datosMonedasPeso();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.red[400],
        title: const Text('Inventario'),
      ),
      backgroundColor: Colors.deepPurple[800],
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            color: Colors.deepPurple[900],
            height: size.height * 0.45,
            child:
              Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [ 
                        Column(
                          children: [
                            Text("PPt", style: TextStyle(color: Colors.white),),
                              Container(
                                width: size.width * 0.25,
                                child: 
                                  TextField(
                                      readOnly: true,
                                      style: TextStyle(color: Colors.white),
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.white, width: 2.0),
                                            borderRadius: BorderRadius.circular(10.0),
                                        ),
                                      ),
                                      controller: pptCntrl,
                              )
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Text("PO", style: TextStyle(color: Colors.yellow[600]),),
                              Container(
                                width: size.width * 0.25,
                                child: 
                                  TextField(
                                      readOnly: true,
                                      style: TextStyle(color: Colors.yellow[600]),
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.yellow[600], width: 2.0),
                                            borderRadius: BorderRadius.circular(10.0),
                                        ),
                                      ),
                                      controller: poCntrl,
                              )
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Text("PE", style: TextStyle(color: Colors.lightBlue[200])),
                              Container(
                                width: size.width * 0.25,
                                child: 
                                  TextField(
                                      readOnly: true,
                                      style: TextStyle(color: Colors.lightBlue[200]),
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.lightBlue[200], width: 2.0),
                                            borderRadius: BorderRadius.circular(10.0),
                                        ),
                                      ),
                                      controller: peCntrl,
                              )
                            )
                          ],
                        ),
                    ],
                  ),
                  SizedBox(height: size.height * 0.025),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [ 
                        Column(
                          children: [
                            Text("PP", style: TextStyle(color: Colors.grey[300])),
                              Container(
                                width: size.width * 0.25,
                                child: 
                                  TextField(
                                      readOnly: true,
                                      style: TextStyle(color: Colors.grey[300]),
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.grey[300], width: 2.0),
                                            borderRadius: BorderRadius.circular(10.0),
                                        ),
                                      ),
                                      controller: ppCntrl,
                              )
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Text("PC", style: TextStyle(color: Colors.deepOrange[400])),
                              Container(
                                width: size.width * 0.25,
                                child: 
                                  TextField(
                                      readOnly: true,
                                      style: TextStyle(color: Colors.deepOrange[400]),
                                      textAlign: TextAlign.center,
                                      decoration: InputDecoration(
                                        enabledBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.deepOrange[400], width: 2.0),
                                            borderRadius: BorderRadius.circular(10.0),
                                        ),
                                      ),
                                      controller: pcCntrl,
                              )
                            )
                          ],
                        ),
                    ],
                  ),
                  SizedBox(height: size.height * 0.05),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 30,
                        width: 200,
                        child: 
                          Row(
                            children: [
                              Image.asset("assets/icons/peso.png"),
                              Text(" : $peso", style: TextStyle(color: Colors.white, fontSize: 25),),
                            ],
                          )
                      ),
                      ElevatedButton(
                        child: 
                          Text(
                            "Monedas", 
                            style: 
                              TextStyle(
                                fontSize: 12
                              ),
                          ),
                        style: 
                          ElevatedButton.styleFrom(
                            primary:
                              Colors.amber[700],
                            fixedSize: 
                              Size(15, 15), 
                            padding: 
                              EdgeInsets.all(2)),
                        onPressed: () {
                          showDialog(context: context, builder: (BuildContext context){
                            return  StatefulBuilder(
                              builder: (context, setState) {
                                return AlertDialog(
                                  title: Text("Monedas"),
                                  contentPadding: EdgeInsets.fromLTRB(40, 40, 40, 20),
                                  content: SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        Form(
                                          key: _formKey,
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              DropdownButtonFormField(
                                                decoration: InputDecoration(labelText: "Tipo de moneda:\n\n", labelStyle: TextStyle(fontWeight: FontWeight.bold)),
                                                value: tipoMoneda,
                                                items: monedas, 
                                                hint: Text("Selecciona"), 
                                                onChanged: (value){
                                                  setState(() {
                                                    tipoMoneda = value;
                                                    if(tipoMoneda != ""){
                                                      cantidadActivo = true;
                                                    }else {
                                                      cantidadActivo = false;
                                                    }
                                                    
                                                  });
                                              }),
                                              SizedBox(height: size.height * 0.02),
                                              Text("Cantidad", style: TextStyle(color: Colors.yellow[700])),
                                              SizedBox(height: size.height * 0.02),
                                              TextFormField(
                                                enabled: cantidadActivo,
                                                onChanged: (value) {
                                                  setState(() {
                                                    
                                                  });
                                                },
                                                validator: (value) {
                                                  if (value == null || value == "" || value.isEmpty) {
                                                    return "Campo obligatorio";
                                                  }
                                                  return null;
                                                },
                                                textAlign: TextAlign.center, 
                                                controller: cantidad, 
                                                keyboardType: TextInputType.number, 
                                                decoration: InputDecoration(
                                                  enabledBorder: OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                      color: Colors.yellow[700],
                                                      width: 2.0,
                                                    ),
                                                    borderRadius: BorderRadius.circular(10)
                                                  ),
                                                  focusedBorder: OutlineInputBorder(
                                                    borderSide: BorderSide(
                                                      color: Colors.yellow[700],
                                                      width: 2.0,
                                                    ),
                                                    borderRadius: BorderRadius.circular(10)
                                                  )
                                                ),
                                                style: TextStyle(color: Colors.yellow[700]),
                                              ),
                                              SizedBox(height: size.height * 0.02),
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  ElevatedButton(
                                                    child: 
                                                      Text(
                                                        "Sumar", 
                                                        style: 
                                                          TextStyle(
                                                            fontSize: 12
                                                          ),
                                                      ),
                                                    style: 
                                                      ElevatedButton.styleFrom(
                                                        primary:
                                                          Colors.green,
                                                        fixedSize: 
                                                          Size(15, 15), 
                                                        padding: 
                                                          EdgeInsets.all(2)),
                                                    onPressed: () async{
                                                      if (_formKey.currentState.validate()) {
                                                        final res = await sumarMonedas(widget.idPj, int.parse(cantidad.text), tipoMoneda);
                                                        if(res["error"] != null){
                                                          showToast(res["error"]);
                                                        }else {
                                                          setState((){
                                                            switch (tipoMoneda) {
                                                              case "ppt":
                                                                  pptCntrl.text = (int.parse(pptCntrl.text) + int.parse(cantidad.text)).toString();
                                                                  break;
                                                              case "po":
                                                                  poCntrl.text = (int.parse(poCntrl.text) + int.parse(cantidad.text)).toString();
                                                                  break;
                                                              case "pe":
                                                                  peCntrl.text = (int.parse(peCntrl.text) + int.parse(cantidad.text)).toString();
                                                                  break;
                                                              case "pp":
                                                                  ppCntrl.text = (int.parse(ppCntrl.text) + int.parse(cantidad.text)).toString();
                                                                  break;
                                                              case "pc":
                                                                  pcCntrl.text = (int.parse(pcCntrl.text) + int.parse(cantidad.text)).toString();
                                                                  break;
                                                            }
                                                          });
                                                          showToast(res["succesful"]);
                                                          Navigator.pop(context);
                                                        }
                                                      }
                                                    },
                                                  ),
                                                  ElevatedButton(
                                                    child: 
                                                      Text(
                                                        "Restar", 
                                                        style: 
                                                          TextStyle(
                                                            fontSize: 12
                                                          ),
                                                      ),
                                                    style: 
                                                      ElevatedButton.styleFrom(
                                                        primary:
                                                          Colors.red,
                                                        fixedSize: 
                                                          Size(15, 15), 
                                                        padding: 
                                                          EdgeInsets.all(2)),
                                                    onPressed: () async{
                                                      if (_formKey.currentState.validate()) {
                                                        final res = await restarMonedas(widget.idPj, int.parse(cantidad.text), tipoMoneda);
                                                        if(res["error"] != null){
                                                          showToast(res["error"]);
                                                        }else {
                                                          var total = 0;
                                                          setState((){
                                                            switch (tipoMoneda) {
                                                              case "ppt":
                                                                  total = (int.parse(pptCntrl.text) - int.parse(cantidad.text));
                                                                  if(total < 0){
                                                                    pptCntrl.text = "0";
                                                                  }else {
                                                                    pptCntrl.text = total.toString();
                                                                  }
                                                                  break;
                                                              case "po":
                                                                  total = (int.parse(poCntrl.text) - int.parse(cantidad.text));
                                                                  if(total < 0){
                                                                    poCntrl.text = "0";
                                                                  }else {
                                                                    poCntrl.text = total.toString();
                                                                  }
                                                                  break;
                                                              case "pe":
                                                                  total = (int.parse(peCntrl.text) - int.parse(cantidad.text));
                                                                  if(total < 0){
                                                                    peCntrl.text = "0";
                                                                  }else {
                                                                    peCntrl.text = total.toString();
                                                                  }
                                                                  break;
                                                              case "pp":
                                                                  total = (int.parse(ppCntrl.text) - int.parse(cantidad.text));
                                                                  if(total < 0){
                                                                    ppCntrl.text = "0";
                                                                  }else {
                                                                    ppCntrl.text = total.toString();
                                                                  }
                                                                  break;
                                                              case "pc":
                                                                  total = (int.parse(pcCntrl.text) - int.parse(cantidad.text));
                                                                  if(total < 0){
                                                                    pcCntrl.text = "0";
                                                                  }else {
                                                                    pcCntrl.text = total.toString();
                                                                  }
                                                                  break;
                                                            }
                                                          });
                                                          showToast(res["succesful"]);
                                                          Navigator.pop(context);
                                                        }
                                                      }
                                                    },
                                                  )
                                                ],
                                              )
                                            ]
                                          )
                                        )
                                      ]
                                    )
                                  )
                                );
                            });
                          });
                        },
                      ), 
                    ]
                  )
                ],
              )
          ),
          Expanded(
            child:
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        IconButton(
                          onPressed: () {
                            Route route = MaterialPageRoute(builder: (context) => ObjetosScreen(widget.idPj, 0));
                            Navigator.push(context, route).then(refresh);
                          }, 
                          iconSize: 90,
                          icon: 
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),//or 15.0
                              child: Container(
                                padding: EdgeInsets.all(10),
                                height: 90.0,
                                width: 90.0,
                                color: Colors.white24,
                                child: Image.asset("assets/icons/pouch.png"),
                              ),
                            ),
                        ),
                        Text("General", style: TextStyle(color: Colors.white))
                      ]
                    ),
                    Column(
                      children: [
                        IconButton(
                          onPressed: () {
                            Route route = MaterialPageRoute(builder: (context) => ObjetosScreen(widget.idPj, 1));
                            Navigator.push(context, route).then(refresh);
                          }, 
                          iconSize: 90,
                          icon: 
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),//or 15.0
                              child: Container(
                                padding: EdgeInsets.all(10),
                                height: 90.0,
                                width: 90.0,
                                color: Colors.white24,
                                child: Image.asset("assets/icons/potion.png"),
                              ),
                            ),
                        ),
                        Text("Pociones", style: TextStyle(color: Colors.white),)
                      ]
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        IconButton(
                          onPressed: () {
                            Route route = MaterialPageRoute(builder: (context) => ObjetosScreen(widget.idPj, 2));
                            Navigator.push(context, route).then(refresh);
                          }, 
                          iconSize: 90,
                          icon: 
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),//or 15.0
                              child: Container(
                                padding: EdgeInsets.all(10),
                                height: 90.0,
                                width: 90.0,
                                color: Colors.white24,
                                child: Image.asset("assets/icons/staff.png"),
                              ),
                            ),
                        ),
                        Text("Objetos Mágicos", style: TextStyle(color: Colors.white))
                      ]
                    ),
                    Column(
                      children: [
                        IconButton(
                          onPressed: () {
                            Route route = MaterialPageRoute(builder: (context) => ObjetosScreen(widget.idPj, 3));
                            Navigator.push(context, route).then(refresh);
                          }, 
                          iconSize: 90,
                          icon: 
                            ClipRRect(
                              borderRadius: BorderRadius.circular(20.0),//or 15.0
                              child: Container(
                                padding: EdgeInsets.all(10),
                                height: 90.0,
                                width: 90.0,
                                color: Colors.white24,
                                child: Image.asset("assets/icons/swords.png"),
                              ),
                            ),
                        ),
                        Text("Armamento", style: TextStyle(color: Colors.white),)
                      ]
                    )
                  ],
                )
              ],
            )
          ),
        ],
      )
    );
  }
}