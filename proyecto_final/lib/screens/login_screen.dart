import 'package:flutter/material.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:proyecto_final/components/show_toast.dart';
import 'package:proyecto_final/data/login_service.dart';
import 'package:proyecto_final/screens/home_screen.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  bool saving = false;
  TextEditingController usuario = new TextEditingController();
  TextEditingController newUsuario = new TextEditingController();
  TextEditingController password = new TextEditingController();
  TextEditingController newPassword = new TextEditingController();
  TextEditingController emailCtrl = new TextEditingController();
  TextEditingController newEmailCtrl = new TextEditingController();
  bool obscureText = true;

  void _togglevisibility() {
    setState(() {
      obscureText = !obscureText;
    });
  }

  Future<void> openAlert(BuildContext context, Size size) async {
    newUsuario.clear();
    newEmailCtrl.clear();
    newPassword.clear();
    return await showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            contentPadding: EdgeInsets.fromLTRB(40, 40, 40, 20),
            content: SingleChildScrollView(
              child: Column(children: [
                Text(
                  "Nombre:",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                 TextFormField(
                  controller: newUsuario,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: size.height * 0.06),
                Text(
                  "Correo electrónico:",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                TextFormField(
                  controller: newEmailCtrl,
                  decoration: InputDecoration(hintText: "micorreo@gmail.com"),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: size.height * 0.06),
                Text(
                  "Contraseña:",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                TextFormField(
                  controller: newPassword,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: size.height * 0.06),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                  ElevatedButton(
                      child: Text("Guardar"),
                      onPressed: () async{
                        final res = await registrarUser(newUsuario.text, newEmailCtrl.text, newPassword.text);
                        if(res["error"] != null){
                          showToast(res["error"]);
                        }else {
                          Navigator.pop(context);
                          showToast(res["msg"]);
                        }
                      }
                  ),
                  ElevatedButton(
                      child: Text("Cancelar"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      }
                  )

                ],)
              ]),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: ModalProgressHUD(
        inAsyncCall: saving,
          child: SingleChildScrollView(
            child:
              Container(
                height: size.height,
                width: size.width,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/mapa.jpg"),
                    fit: BoxFit.cover
                  )
                ),
                child: Container( 
                  //margin: EdgeInsets.fromLTRB(40, 0, 40, 0),
                  color: Colors.white.withOpacity(0.3),
                  child: 
                    Padding(
                      padding: EdgeInsets.fromLTRB(40, 0, 40, 0),
                      child:
                        Column(
                          children: [
                            SizedBox(height: size.height * 0.14),
                            Image.asset(
                              'assets/icons/icosahedron.png',
                              width: 140,
                              height: 140,
                            ),
                            SizedBox(height: size.height * 0.06),
                            TextField(
                              controller: usuario, 
                              decoration: InputDecoration(
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                prefixIcon: Icon(Icons.person, color: Colors.black,),
                                hintText: "Usuario",
                                filled: true,
                                fillColor: Colors.white38
                              ),
                              
                            ),
                            SizedBox(height: size.height * 0.06),
                            TextField(
                              controller: password, 
                              decoration: InputDecoration(
                                suffixIcon: GestureDetector(
                                  onTap: (){
                                    _togglevisibility();
                                  },
                                  child: Icon(
                                    obscureText ? Icons.visibility : Icons
                                          .visibility_off, color: Colors.red),
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                prefixIcon: Icon(Icons.lock, color: Colors.black,),
                                hintText: "Contraseña",
                                filled: true,
                                fillColor: Colors.white38
                              ),
                              obscureText: obscureText,
                            ),
                            SizedBox(height: size.height * 0.06),
                            ElevatedButton(
                              onPressed: () async{
                                if (usuario.text != '' && password.text != '') {
                                  setState(() {
                                    saving = true;
                                  });
                                  final res =
                                      await iniciaSesion(usuario.text, password.text);
                                  setState(() {
                                    saving = false;
                                  });
                                  if (res == 'successful') {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (contex) => HomeScreen()
                                      ),
                                    );
                                  } else {
                                    showToast(res);
                                  }
                                } else {
                                  showToast('Es necesario rellenar todos los campos.');
                                }
                              }, 
                              child: Text("Iniciar Sesión")
                            ),
                            SizedBox(height: size.height * 0.02),
                            ElevatedButton(
                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.amber[800]),),
                              onPressed: () {
                                usuario.clear();
                                password.clear();
                                openAlert(context, size);
                              },
                              child: Text("Registrarse")
                            )
                          ],
                        ), 
                    )
                  
                ),
              ),
          ),
        ),
      //),
    );
  }
}