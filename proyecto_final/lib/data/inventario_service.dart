import 'package:proyecto_final/routes_api_http/webclient.dart';

Future traerMonedasPeso(id) async {
  final res = await datosMonedasPeso(id);
  return res;
}
Future sumarMonedas(id, cantidad, moneda) async {
  final res = await sumar(id, cantidad, moneda);
  return res;
}
Future restarMonedas(id, cantidad, moneda) async {
  final res = await restar(id, cantidad, moneda);
  return res;
}
Future anyadirObjeto(idPj, idPag, nombre, descripcion, valor, moneda, peso) async {
  final res = await addObjeto(idPj, idPag, nombre, descripcion, valor, moneda, peso);
  return res;
}
Future editarObjeto(idObj, nombre, descripcion, valor, moneda, peso) async {
  final res = await editObjeto(idObj, nombre, descripcion, valor, moneda, peso);
  return res;
}
Future eliminarObjeto(idObj) async {
  final res = await removeObjeto(idObj);
  return res;
}
Future getDatosObj(idPj, idPag) async {
  final res = await traerDatosObj(idPj, idPag);
  return res;
}