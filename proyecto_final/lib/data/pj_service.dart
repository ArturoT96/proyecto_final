import 'package:proyecto_final/routes_api_http/webclient.dart';

Future curacionPJ(id, vida) async {
  final res = await curar(id, vida);
  return res;
}
Future addTemp(id, temp) async {
  final res = await addTemporales(id, temp);
  return res;
}
Future vidayTemp(id, vida, temp) async {
  final res = await saludyTemp(id, vida, temp);
  return res;
}

/*Future editarEstado(id, key, estado) async {
  final res = await editEstado(id, key, estado);
  return res;
}*/
Future editarEstados(id, estados) async {
  final res = await editEstados(id, estados);
  return res;
}

Future traerEstados(id) async {
  final res = await getEstados(id);
  return res;
}