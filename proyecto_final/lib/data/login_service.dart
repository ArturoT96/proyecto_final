import 'package:proyecto_final/routes_api_http/webclient.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future iniciaSesion(usuario, password) async {
  var user = usuario.trim();
  var pass = password.trim();
  final res = await login(user, pass);
  print("respuesta -> $res");
  if (res['error'] != null) {
    return res['error'];
  }

  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('token', res['access_token']);
  prefs.setString('name', user);

  return 'successful';
}

Future registrarUser(usuario, email, password) async {
  var user = usuario.trim();
  var correo = email.trim();
  var pass = password.trim();
  final res = await register(user, correo, pass);

  return res;
}

getTokenValue() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String tokenValue = prefs.getString('token');
  return tokenValue;
}


getUserValue() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String userValue = prefs.getString('name');
  return userValue;
}
