import 'package:proyecto_final/routes_api_http/webclient.dart';

Future<Map<String, dynamic>> getDiario(id) async {
  final res = await getDatosDiario(id);
  return res;
}

Future addPestanya(id, nombre, new_pag) async {
  final res = await anyadirPestanya(id, nombre, new_pag);
  return res;
}

Future editPestanya(id, oldName, newName) async {
  final res = await editarPestanya(id, oldName, newName);
  return res;

}
Future deleteTab(id, nombre) async {
  final res = await borrarTab(id, nombre);
  return res;
}
Future deleteNota(id, nombre, currentPage) async {
  final res = await borrarNota(id, nombre, currentPage);
  return res;
}
Future deletePagina(id, nombre, currentPage) async {
  final res = await borrarPagina(id, nombre, currentPage);
  return res;
}
Future saveNota(id, nombre, currentPage, nota) async {
  final res = await guardarNota(id, nombre, currentPage, nota);
  return res;
}

Future searchWord(id, palabra) async {
  final res = await buscarPalabra(id, palabra);
  return res;
}

Future crearPestanya(id) async {
  final res = await createPestanya(id);
  return res;
}
Future addPagina(id, nombre) async {
  final res = await anyadirPagina(id, nombre);
  return res;
}