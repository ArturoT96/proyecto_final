import 'package:proyecto_final/routes_api_http/webclient.dart';

Future newPJ(foto, nombre, clase, nivel, campanya, vida, clase_armadura, userName) async {
  final res = await anyadirPJ(foto, nombre, clase, nivel, campanya, vida, clase_armadura, userName);
  return res;
}

Future editarPJ(foto, nombre, clase, nivel, campanya, vida, clase_armadura, id) async {
  final res = await editPJ(foto, nombre, clase, nivel, campanya, vida, clase_armadura, id);
  return res;
}

Future getDatosPJ(userName) async{
  final res = await traerDatosPJ(userName);
  return res;
}

Future borrarPJ(id) async{
  final res = await deletePJ(id);
  return res;
}
