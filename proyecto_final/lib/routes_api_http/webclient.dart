import 'dart:convert';
import 'package:http/http.dart';
import 'package:http_interceptor/http_interceptor.dart';

class LoggingInterceptor implements InterceptorContract {
  @override
  Future<RequestData> interceptRequest({RequestData data}) async {
    return data;
  }

  @override
  Future<ResponseData> interceptResponse({ResponseData data}) async {
    print('Response');
    print('url: ${data.url}');
    print('headers: ${data.headers}');
    print('status: ${data.statusCode}');
    print('body: ${data.body}');
    print('---------- END Response -----------------');
    return data;
  }
}

final Client client = InterceptedClient.build(
  interceptors: [LoggingInterceptor()],
);

const String baseUrl = 'http://192.168.100.20:8000/api/';

Future<Map<String, dynamic>> login(usuario, password) async {
  final Map<String, dynamic> dataMap = {
    'name': usuario,
    'password': password,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'login'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>> register(usuario, email, password) async {
  final Map<String, dynamic> dataMap = {
    'name': usuario,
    'email': email,
    'password': password,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'register'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>> anyadirPJ(foto, nombre, clase, nivel, campanya, vida, clase_armadura, userName) async {
  final Map<String, dynamic> dataMap = {
    'foto': foto,
    'nombre': nombre,
    'clase': clase,
    'nivel': nivel,
    'campanya': campanya,
    'vida': vida,
    'clase_armadura': clase_armadura,
    'userName': userName,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'addPJ'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>> editPJ(foto, nombre, clase, nivel, campanya, vida, clase_armadura, id) async {
  final Map<String, dynamic> dataMap = {
    'foto': foto,
    'nombre': nombre,
    'clase': clase,
    'nivel': nivel,
    'campanya': campanya,
    'vida': vida,
    'clase_armadura': clase_armadura,
    'id': id,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'editPJ'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>> traerDatosPJ(userName) async {
  final Map<String, dynamic> dataMap = {
    'userName': userName,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'traerDatos'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>> deletePJ(id) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'deletePJ'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>> curar(id, vida) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'vida': vida,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'curarPJ'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>> addTemporales(id, temp) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'temp': temp ,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'addTemp'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>> saludyTemp(id, vida, temp) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'vida': vida,
    'temp': temp,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'saludyTemp'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

/*Future<Map<String, dynamic>> editEstado(id, key, estado) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'key': key,
    'estado': estado,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'editEstado'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}*/
Future<Map<String, dynamic>> getEstados(id) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'getEstados'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>> editEstados(id, estados) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'estados': estados,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'editEstados'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>> getDatosDiario(id) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'getDiario'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  anyadirPestanya(id, nombre, new_pag) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'nombrePestanya': nombre,
    'new_pag': new_pag,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'addPestanya'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  editarPestanya(id, oldName, newName) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'newName': newName,
    'oldName': oldName,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'editPestanya'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  borrarTab(id, nombre) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'nombre': nombre
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'deleteTab'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  borrarNota(id, nombre, currentPage) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'nombre': nombre,
    'currentPage': currentPage
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'deleteNota'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  borrarPagina(id, nombre, currentPage) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'nombre': nombre,
    'currentPage': currentPage
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'deletePagina'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  guardarNota(id, nombre, currentPage, nota) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'nombre': nombre,
    'currentPage': currentPage,
    'nota': nota
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'saveNota'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  buscarPalabra(id, palabra) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'palabra': palabra
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'searchWord'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  createPestanya(id) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'createPestanya'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  anyadirPagina(id, nombre) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'nombrePestanya': nombre,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'anyadirPagina'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  datosMonedasPeso(id) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'datosMonedasPeso'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  sumar(id, cantidad, moneda) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'cantidad': cantidad,
    'moneda': moneda,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'sumarMonedas'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

Future<Map<String, dynamic>>  restar(id, cantidad, moneda) async {
  final Map<String, dynamic> dataMap = {
    'id': id,
    'cantidad': cantidad,
    'moneda': moneda,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'restarMonedas'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>>  addObjeto(idPj, idPag, nombre, descripcion, valor, moneda, peso) async {
  final Map<String, dynamic> dataMap = {
    'idPj': idPj,
    'idPag': idPag,
    'nombre': nombre,
    'descripcion': descripcion,
    'valor': valor,
    'moneda': moneda,
    'peso': peso,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'addObjeto'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>>  editObjeto(idObj, nombre, descripcion, valor, moneda, peso) async {
  final Map<String, dynamic> dataMap = {
    'idObj': idObj,
    'nombre': nombre,
    'descripcion': descripcion,
    'valor': valor,
    'moneda': moneda,
    'peso': peso,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'editObjeto'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>>  removeObjeto(idObj) async {
  final Map<String, dynamic> dataMap = {
    'idObj': idObj,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'removeObjeto'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}
Future<Map<String, dynamic>>  traerDatosObj(idPj, idPag) async {
  final Map<String, dynamic> dataMap = {
    'idPj': idPj,
    'idPag': idPag,
  };
  final String dataJson = jsonEncode(dataMap);

  final Response response = await client.post(Uri.parse(baseUrl + 'traerDatosObj'),
      headers: {'Content-type': 'application/json'}, body: dataJson);

  Map<String, dynamic> json = jsonDecode(response.body);

  return json;
}

