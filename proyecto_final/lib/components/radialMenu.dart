import 'package:flutter/material.dart';
import 'dart:math';
import 'package:vector_math/vector_math.dart' show radians;


class RadialMenu extends StatefulWidget {

  createState() => _RadialMenuState();
}

class _RadialMenuState extends State<RadialMenu> with SingleTickerProviderStateMixin {
  
  AnimationController controller;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(duration: Duration(milliseconds: 900), vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return RadialAnimation(controller: controller);
  }

}


// The Animation 
class RadialAnimation extends StatelessWidget {
    RadialAnimation({ Key key, this.controller }) : 
    
      scale = Tween<double>(
        begin: 1.5,
        end: 0.0,
      ).animate(
        CurvedAnimation(
          parent: controller,
          curve: Curves.fastOutSlowIn
        ),
      ),

      translation = Tween<double>(
        begin: 0.0,
        end: 75.0,
      ).animate(
        CurvedAnimation(
          parent: controller,
          curve: Curves.linear
        ),
      ),

      rotation = Tween<double>(
        begin: 0.0,
        end: 360.0,
      ).animate(
        CurvedAnimation(
          parent: controller,
          curve: Interval(
            0.3, 0.9,
            curve: Curves.decelerate,
          ),
        ),
      ),
    
    super(key: key);

    final AnimationController controller;
    final Animation<double> scale;
    final Animation<double> translation;
    final Animation<double> rotation;

    _buildButton(double angle, {String icon, String btnTag }) {
      final double rad = radians(angle);
      return Transform(
        transform: Matrix4.identity()..translate(
          (translation.value) * cos(rad), 
          (translation.value) * sin(rad)
        ),

        child: 
          ClipRRect(
            borderRadius: BorderRadius.circular(60),
            child:
              FloatingActionButton(
                heroTag: btnTag,
                child: Image.asset(icon), onPressed: _close, elevation: 0)
              )
          );
    }


    build(context) {
      return AnimatedBuilder(
        animation: controller, 
        builder: (context, builder) {
          return Transform.rotate( // Add rotation
            angle: radians(rotation.value),
            child:
              Stack(
                alignment: Alignment.center,
                children: [
                  _buildButton(0, icon: "assets/icons/dice/d4.png", btnTag: "btn4"),
                  _buildButton(60, icon: "assets/icons/dice/d6.png", btnTag: "btn6"),
                  _buildButton(120, icon: "assets/icons/dice/d8.png", btnTag: "btn8"),
                  _buildButton(180, icon: "assets/icons/dice/d10.png", btnTag: "btn10"),
                  _buildButton(240, icon: "assets/icons/dice/d12.png", btnTag: "btn12"),
                  _buildButton(300, icon: "assets/icons/dice/d20.png", btnTag: "btn20"),

                  Transform.scale(
                    scale: scale.value - 1.5, // subtract the beginning value to run the opposite animation
                    child: Container(height: 35, width: 35, child:FloatingActionButton(
                      heroTag: "btn2",
                      child: Icon(Icons.close), 
                      onPressed: _close, 
                      backgroundColor: Colors.red
                    )),
                  ),
                  Transform.scale(
                    scale: scale.value,
                    child: FloatingActionButton(
                      heroTag: "btn1",
                      backgroundColor: Colors.black,
                      child: CircleAvatar(child: Image.asset("assets/icons/dice/rolling-dices.png")), 
                      onPressed: _open
                    )
                  ),
                ]
              )
            );
      });
    }

    _open() {
      controller.forward();
      
    }

    _close() {
      controller.reverse();
    }
}